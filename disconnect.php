<?php

// Remise à 0 de la session pour la déconnexion
$_SESSION = array();
session_destroy();

// Redirection vers la page d'accueil
header("Location: index.php?page=home");
exit();

?>
