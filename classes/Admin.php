<?php

class Admin
{
    private $id;
    private $username;
    private $password;
    private $role;

// Getter et setter

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setRole($role)
    {
        $this->role = $role;
    }

    public function getRole()
    {
        return $this->role;
    }
// Fin des Getter et Setter

    // Connexion de l'admin avec vérification que le nom d'utilisateur existe et que le mot de passe entré soit le bon
    // Redirection vers la page admin après la connexion
    // En cas d'erreur, la fonction retourne false afin d'indiquer un message d'erreur dans la partie front
    public function login()
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $results = $dbh->prepare('SELECT * from `admins` WHERE `username` = ?');
        $results->execute(array($this->getUsername()));
        $nbr = $results->rowCount();

        if ($nbr != 0) {
            $data = $results->fetch(PDO::FETCH_ASSOC);

            // Vérification sur le mot de passe crypté
            $pass_verif = password_verify($this->getPassword(), $data["password"]);

            if (!$pass_verif) {
               return false;
            } else {
                $this->setRole($data["role"]);
                $this->setId($data["id"]);
                $this->setPassword(null);
                $_SESSION["logged"] = $this;
                header("Location: index.php?page=admin");
                exit();

            }

        } else {
            return false;
        }
    }


    // Récupération de tous les admins dans un tableau d'objets
    // Les admins sont ordonnés par role, d'abord les administrateurs puis les éditeurs afin de les regrouper par catégorie
    public function getAll()
    {
        $result = array();
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->query('SELECT * FROM `admins` WHERE `role` != 2 ORDER BY `role`');

        while ($data = $sth->fetchObject('Admin')) {
            array_push($result, $data);
        }

        return $result;
    }

    // Récupération d'un admin grâce à son id
    public function getAdmin($id)
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('SELECT * FROM `admins` WHERE `id` = ?');
        $result = $sth->execute(array($id));
        $data = $sth->fetch(PDO::FETCH_OBJ);
        if ($data) {
            $this->setUsername($data->username);
            $this->setRole($data->role);
            $this->setId($id);
        }
    }

    // Ajout d'un admin
    public function addAdmin(&$valid)
    {
        $username = $this->getUsername();
        $password = $this->getPassword();
        $role = $this->getRole();

        $BDD = new BDD();
        $dbh = $BDD->getConnection();

        // Vérification de l'existence du nom d'utilisateur proposé dans la BDD
        $sth = $dbh->prepare('SELECT `username` FROM `admins` WHERE `username` = ?');
        $result = $sth->execute(array($this->getUsername()));
        $data = $sth->fetch(PDO::FETCH_OBJ);
        // S'il n'existe pas déjà, il est enregistré
        if (!$data) {

            $stmt = $dbh->prepare("INSERT INTO `admins` (`username`, `password`, `role`) VALUES (:uname, :pass, :ro)");
            $stmt->bindParam(':uname', $username);
            $stmt->bindParam(':pass', $password);
            $stmt->bindParam(':ro', $role);
            $stmt->execute();
        } else {
            // Sinon il renvoit le paramètre envoyé en référence avec un nouvelle valeur
            $valid = "username";
        }
    }

    // Modification d'un admin
    public function updateAdmin(&$valid)
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();

        // Vérification de l'existence du nom d'utilisateur proposé dans la BDD avec un id différent
        $sth = $dbh->prepare('SELECT `username` FROM `admins` WHERE `username` = :uname AND `id` != :id');
        $result = $sth->execute(array(':uname' => $this->getUsername(), ':id' => $this->getId()));
        $data = $sth->fetch(PDO::FETCH_OBJ);

        // S'il n'existe pas déjà, il est modifié
        if (!$data) {
            $stmt = $dbh->prepare('UPDATE `admins` SET `username` = :uname, `role` = :nro WHERE `id` = :id');
            $stmt->execute(array(':uname' => $this->getUsername(), ':nro' => $this->getRole(), ':id' => $this->getId()));

        } else {
            // Sinon il renvoit le paramètre envoyé en référence avec un nouvelle valeur
            $valid = "username";
        }

    }

    // Suppression d'un admin
    public function deleteAdmin()
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('DELETE FROM `admins` WHERE `id` = :id');
        $sth->execute(array(':id' => $this->getId()));
    }

}
