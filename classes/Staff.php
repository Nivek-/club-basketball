<?php 
// require_once("autoload.php");
class Staff {
    private $id;
    private $lastname;
    private $firstname;
    private $role;
    private $position;
    public $translatedPosition;
    public $roleName;
    


    // Getter et setter

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }


    public function setLastname($lastname) {
        $this->lastname = $lastname;
    }

    public function getLastname() {
        return $this->lastname;
    }


    public function setFirstname($firstname) {
        $this->firstname = $firstname;
    }

    public function getFirstname() {
        return $this->firstname;
    }


    public function setRole($role) {
        $this->role = $role;
    }

    public function getRole() {
        return $this->role;
    }


    public function setPosition($position) {
        $this->position = $position;
    }

    public function getPosition() {
        return $this->position;
    }

    // Fin des Getter et Setter


    // Récupération de tous les membres de l'équipe dans un tableau d'objets 
    // Les membres sont ordonnés par role, d'abord les joueuses puis les coach afin de les regrouper par catégorie
    public function getAll() {
        $result = array();
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->query('SELECT * FROM `staff` ORDER BY `role`');

        while ($data = $sth->fetchObject('Staff')) {
            array_push($result,$data);
        }
        
        return $result;
    }

    // Récupération d'un membre grâce à son id
    public function getStaffMember($id) {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('SELECT * FROM `staff` WHERE `id` = ?');
        $result = $sth->execute(array($id));
        $data = $sth->fetch(PDO::FETCH_OBJ);
        if ($data) {
            $this->setLastname($data->lastname);
            $this->setFirstname($data->firstname);
            $this->setRole($data->role);
            $this->setPosition($data->position);
            $this->setId($id);
        }
    }

    // Ajout d'un membre
    public function addStaff() {
        $lastname = $this->getLastname();
        $firstname = $this->getFirstname();
        $role = $this->getRole();
        $position = $this->getPosition();

        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $stmt = $dbh->prepare("INSERT INTO staff (`lastname`,`firstname`, `role`, `position`) VALUES (:lname, :fname, :ro, :po)");
        $stmt->bindParam(':lname', $lastname);
        $stmt->bindParam(':fname', $firstname);
        $stmt->bindParam(':ro', $role);
        $stmt->bindParam(':po', $position);
        $stmt->execute();
    }

    // Modification d'un membre
    public function updateStaff() {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $stmt = $dbh->prepare('UPDATE `staff` SET `lastname` = :nlname, `firstname` = :nfname, `role` = :nro, `position` = :npos WHERE `id` = :id');
        $stmt->execute(array(':nlname' => $this->getLastname(), ':nfname' => $this->getFirstname(), ':nro' => $this->getRole(), ':npos' => $this->getPosition(), ':id' => $this->getId()));

    }

    // Suppression d'un membre
    public function deleteStaff() {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('DELETE FROM `staff` WHERE `id` = :id');
        $sth->execute(array(':id' => $this->getId()));
    }



    public function translatePosition() {
        switch ($this->getPosition()) {
            case "point_guard":
                $this->translatedPosition = "meneur";
                break;
            case "shooting_guard":
                $this->translatedPosition = "arrière";
                break;
            case "small_forward":
                $this->translatedPosition = "ailier";
                break;
            case "power_forward":
                $this->translatedPosition = "ailier fort";
                break;
            case "center":
                $this->translatedPosition = "pivot";
                break;
        }
    }
}