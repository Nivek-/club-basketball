<?php

class Article {
    private $id;
    private $title;
    private $content;
    private $date;
    private $publish;
    private $image;

// Getter et setter

public function setId($id) {
    $this->id = $id;
}

public function getId() {
    return $this->id;
}


public function setTitle($title) {
    $this->title = $title;
}

public function getTitle() {
    return $this->title;
}


public function setContent($content) {
    $this->content = $content;
}

public function getContent() {
    return $this->content;
}


public function setArticleDate($date) {
    $this->date = $date;
}

public function getArticleDate() {
    return $this->date;
}


public function setPublish($publish) {
    $this->publish = $publish;
}

public function getPublish() {
    return $this->publish;
}

public function setImage($image) {
    $this->image = $image;
}

public function getImage() {
    return $this->image;
}

// Fin des Getter et Setter


    // Récupération de tous les articles dans un tableau d'objets 
    // Les articles sont ordonnés par date
    public function getAll() {
        $result = array();
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->query('SELECT * FROM `articles` ORDER BY `date` DESC');

        while ($data = $sth->fetchObject('Article')) {
            array_push($result,$data);
        }
        
        return $result;
    }

    // Récupération d'un article grâce à son id
    public function getArticle($id) {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('SELECT * FROM `articles` WHERE `id` = ?');
        $result = $sth->execute(array($id));
        $data = $sth->fetch(PDO::FETCH_OBJ);
        if ($data) {
            $this->setTitle($data->title);
            $this->setContent($data->content);
            $this->setArticleDate($data->date);
            $this->setPublish($data->publish);
            $this->setImage($data->image);
            $this->setId($id);
            return true;
        } else {
            return false;
        }
    }

    // Ajout d'un article
    public function addArticle() {
        $title = $this->getTitle();
        $content = $this->getContent();
        $date = $this->getArticleDate();
        $publish = $this->getPublish();
        $image = $this->getImage();

        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $stmt = $dbh->prepare("INSERT INTO articles (`title`,`content`, `date`, `publish`, `image`) VALUES (:title, :content, :d, :publish, :i)");
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':content', $content);
        $stmt->bindParam(':d', $date);
        $stmt->bindParam(':publish', $publish);
        $stmt->bindParam(':i', $image);
        $stmt->execute();
    }

    // Modification d'un article
    public function updateArticle() {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $stmt = $dbh->prepare('UPDATE `articles` SET `title` = :title, `content` = :content, `publish` = :publish WHERE `id` = :id');
        $stmt->execute(array(':title' => $this->getTitle(), ':content' => $this->getContent(), ':publish' => $this->getPublish(), ':id' => $this->getId()));

    }

    // Modification d'une image d'un article
    public function updateImage() {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $stmt = $dbh->prepare('UPDATE `articles` SET `image` = :i WHERE `id` = :id');
        $stmt->execute(array(':i' => $this->getImage(), ':id' => $this->getId()));
    }

    // Suppression d'un article
    public function deleteArticle() {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('DELETE FROM `articles` WHERE `id` = :id');
        $sth->execute(array(':id' => $this->getId()));
    }

    public function getLastArticles() {
        $result = array();
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->query('SELECT * FROM `articles` WHERE `publish` = 1 ORDER BY `date` DESC LIMIT 3');
        while ($data = $sth->fetchObject('Article')) {
            array_push($result,$data);
        }
        
        return $result;
    }
}