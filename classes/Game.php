<?php

class Game
{
    private $id;
    private $date;
    private $hour;
    private $opponent;

// Getter et setter

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }


    public function setDateGame($date)
    {
        $this->date = $date;
    }

    public function getDateGame()
    {
        return $this->date;
    }


    public function setHour($hour)
    {
        $this->hour = $hour;
    }

    public function getHour()
    {
        return $this->hour;
    }

    
    public function setOpponent($opponent)
    {
        $this->opponent = $opponent;
    }

    public function getOpponent()
    {
        return $this->opponent;
    }

    // Fin des Getter et Setter


    // Récupération de tous les matchs dans un tableau d'objets 
    public function getAll() {
        $result = array();
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->query('SELECT * FROM `games` ORDER BY `date` ASC');

        while ($data = $sth->fetchObject('Game')) {
            array_push($result,$data);
        }
        
        return $result;
    }


    // Récupération d'un match grâce à son id
    public function getGame($id) {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('SELECT * FROM `games` WHERE `id` = ?');
        $result = $sth->execute(array($id));
        $data = $sth->fetch(PDO::FETCH_OBJ);
        if ($data) {
            $this->setDateGame($data->date);
            $this->setHour($data->hour);
            $this->setOpponent($data->opponent);
            $this->setId($id);
        }
    }

    // Ajout d'un match
    public function addGame() {
        $date = $this->getDateGame();
        $hour = $this->getHour();
        $opponent = $this->getOpponent();

        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $stmt = $dbh->prepare("INSERT INTO `games` (`date`,`hour`, `opponent`) VALUES (:d, :h, :opp)");
        $stmt->bindParam(':d', $date);
        $stmt->bindParam(':h', $hour);
        $stmt->bindParam(':opp', $opponent);
        $stmt->execute();
    }

    // Modification d'un match
    public function updateGame() {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $stmt = $dbh->prepare('UPDATE `games` SET `date` = :d, `hour` = :h, `opponent` = :opp WHERE `id` = :id');
        $stmt->execute(array(':d' => $this->getDateGame(), ':h' => $this->getHour(), ':opp' => $this->getOpponent(), ':id' => $this->getId()));

    }

    // Suppression d'un match
    public function deleteGame() {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('DELETE FROM `games` WHERE `id` = :id');
        $sth->execute(array(':id' => $this->getId()));
    }

    // Récupération du prochain match à venir
    public function getLastGame() {
        $current_date = time();
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('SELECT * FROM `games` WHERE `date` > ? ORDER BY `date` ASC LIMIT 1');
        $result = $sth->execute(array($current_date));
        $data = $sth->fetchObject('Game');
        return $data;
    }
}
