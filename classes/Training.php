<?php 
// require_once("autoload.php");
class Training {
    private $id;
    private $day;
    private $begin;
    private $end;
    private $room;
    private $coach_id;


    // Getter et setter

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }


    public function setDay($day) {
        $this->day = $day;
    }

    public function getDay() {
        return $this->day;
    }


    public function setBegin($begin) {
        $this->begin = $begin;
    }

    public function getBegin() {
        return $this->begin;
    }


    public function setEnd($end) {
        $this->end = $end;
    }

    public function getEnd() {
        return $this->end;
    }


    public function setRoom($room) {
        $this->room = $room;
    }

    public function getRoom() {
        return $this->room;
    }

    public function setCoachId($coach_id) {
        $this->coach_id = $coach_id;
    }

    public function getCoachId() {
        return $this->coach_id;
    }

    // Fin des Getter et Setter

    // Récupération de tous les entraînements dans un tableau d'objets 
    // Le nom et prénom du coach sont récupérés grâce à son id
    public function getAll() {
        $result = array();
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->query('SELECT * from `trainings`');

        while ($data = $sth->fetchObject('Training')) {
            $stmt = $dbh->prepare('SELECT `lastname`, `firstname` FROM `staff` WHERE `id` = ?');
            $stmt->execute(array($data->getCoachId()));
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $data->setCoachId($row["firstname"]." ".strtoupper($row["lastname"]));
            array_push($result,$data);

        }
        
        return $result;
    }

    // Récupération d'un entraînement grâce à son id
    // Le nom et prénom du coach sont récupérés grâce à son id
    public function getTraining($id) {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('SELECT * FROM `trainings` WHERE `id` = ?');
        $result = $sth->execute(array($id));
        $data = $sth->fetch(PDO::FETCH_OBJ);
        if ($data) {
            $this->setDay($data->day);
            $this->setBegin($data->begin);
            $this->setEnd($data->end);
            $this->setRoom($data->room);
            $this->setId($id);

            $stmt = $dbh->prepare('SELECT `lastname`, `firstname` FROM `staff` WHERE `id` = ?');
            $stmt->execute(array($data->coach_id));
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->setCoachId($row["firstname"]." ".strtoupper($row["lastname"]));
        }
    }

    // Ajout d'un entraînement 
    public function addTraining() {
        $day = $this->getDay();
        $begin = $this->getBegin();
        $end = $this->getEnd();
        $room = $this->getRoom();
        $coach_id = $this->getCoachId();

        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $stmt = $dbh->prepare("INSERT INTO trainings (`day`,`begin`, `end`, `room`, `coach_id`) VALUES (:d, :beg, :en, :room, :coach)");
        $stmt->bindParam(':d', $day);
        $stmt->bindParam(':beg', $begin);
        $stmt->bindParam(':en', $end);
        $stmt->bindParam(':room', $room);
        $stmt->bindParam(':coach', $coach_id);
        $stmt->execute();
    }

    // Modification d'un entraînement
    public function updateTraining() {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $stmt = $dbh->prepare('UPDATE `trainings` SET `day` = :d, `begin` = :beg, `end` = :e, `room` = :room, `coach_id` = :coach WHERE `id` = :id');
        $stmt->execute(array(':d' => $this->getDay(), ':beg' => $this->getBegin(), ':e' => $this->getEnd(), ':room' => $this->getRoom(), ':coach' => $this->getCoachId(), ':id' => $this->getId()));

    }

    // Suppression d'un entraînement
    public function deleteTraining() {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('DELETE FROM `trainings` WHERE `id` = :id');
        $sth->execute(array(':id' => $this->getId()));
    }
}