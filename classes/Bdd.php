<?php
class BDD{

    private $dbh;
    // Constructeur de la classe BDD permettant d'accéder à la base de données
    public function __construct() {
        $this->dbh = new \PDO('mysql:host=localhost;dbname=basketball_club', '', '');
    }

    // Getter
    public function getConnection(){
        return $this->dbh;
    }
}
?>