<nav>
	<div id="menu">
		<a href="index.php?page=home"><img src="assets/images/logo_club_feminin_basketball_avesnes.png"
				alt="logo du club féminin de basketball d\Avesnes"></a>
		<ul>
			<li><a class='nav_link' id='history' href="index.php?page=history">Le basket féminin</a></li>
			<li><a class='nav_link' id='team' href="index.php?page=team">Les équipes féminines</a></li>
			<li  ><a class='nav_link' id='tickets' href="index.php?page=tickets">Les tarifs</a></li>
			<li ><a class='nav_link' id='contact' href="index.php?page=contact">Contacts</a></li>
		</ul>
	</div>
	<div id="responsive_menu">
		<i class="fas fa-bars"></i>
		<i class="fas fa-times""></i>
		<div id="list_menu"></div>
	</div>
</nav>
<div class='banner'>
	<img src="assets/images/basketball_field.jpg" alt="ballon de basket sur le parquet d'un terrain de basketball">
	<div class='banner_text'>
		<h1>Club de basketball d’Avesnes  : vivez le basket féminin avec passion</h1>
	</div>
</div>
<div class='last_game'>
	<?php 
	// Importation du prochain match à partir de la date du jour
	$game = new Game();
	$game = $game->getLastGame();

	// Tableaux associatifs contenant les jours et les mois pour les afficher dans la date du match
	$months = array("1" => "janvier", "2"=> "février", "3" => "mars", "4" => "avril", "5" => "mai", "6" => "juin", "7" => "juillet", "8" => "août", "9" => "septembre", "10" => "octobre", "11" => "novembre", "12" => "décembre");
	$week_days = array("1" => "lundi", "2" => "mardi", "3" => "mercredi", "4" => "jeudi", "5" => "vendredi", "6" => "samedi", "7" => "dimanche");
	
	// récupération de la date(format timestamp) au format Date[]
	$date = getDate($game->getDateGame());

	if($date["mday"] < 10) {
		$day = "0".$date["mday"];
	} else {
		$day = $date["mday"];
	}
	
	// Format d'affichage de la date
	$game_date = "Le ".ucfirst($week_days[$date["wday"]])." ".$day." ".$months[$date["mon"]]." ".$date["year"];

	echo "<h2>Prochain match à domicile : BC Avesnes / ".ucfirst($game->getOpponent())." - ".$game_date." à ".str_replace(":", "h",$game->getHour());
	?>
</div>