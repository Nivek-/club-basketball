<!-- LAST NEWS SECTION -->

<div class='container'>
    <h3>Nos dernières actualités</h3>
    <!-- NEWS CARDS -->
    <div class='news'>
        <?php

        // Importation des 3 derniers articles en base de données
        $articles = new Article();
        $article_results = $articles->getLastArticles();

        // Itération sur chaque élément afin d'afficher une carte avec l'image, le titre et une partie du texte
        foreach ($article_results as $result) {
            echo "<div class='news_card'>";
            echo "<div class='img_card'>";
            echo "<img src='assets/upload/".$result->getImage()."' alt='".$result->getTitle()."'>";
            echo "</div>";
            echo "<div class='card_text'>";
            echo "<h5>".ucfirst($result->getTitle())."</h5>";
            echo "<div class='article_content'>";
            echo substr(strip_tags($result->getContent()),0,150)."...";
            echo "</div></div>";
            echo "<a href='index.php?page=article&id=".$result->getId()."' class='page_button'>Lire la suite</a>";
            echo "</div>";
        }
        ?>
    </div>
</div>

<!-- TEAM SECTION -->
<div class='covered_section'>
    <h3>Notre équipe en action</h3>
    <div class='basketball_game'>
    <iframe src="https://www.youtube.com/embed/LRgnM3mUGcA?start=329" frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>

<div class='container'>
    <!-- WOMEN'S BASKETBALL SECTION -->

    <h3>Le basketball féminin</h3>
    <div class='women_basketball'>
        
        <!-- HISTORY CARD -->
        <div class='woman_basket_card'>
            <div class="img_card">
                <img src="assets/images/woman_holding_basketball.jpg" alt="femme aux longs cheveux portant un bonnet tenant un ballon de basket dans la main">
            </div>
            <p>Nulla a sollicitudin mi. Curabitur sed varius nulla. Ut tortor dolor, laoreet vel sem vitae, finibus
                tincidunt nisi. Proin pellentesque gravida nisi, non lobortis ante interdum ut. Sed pulvinar aliquet
                ante,
                sit amet blandit sapien ultrices eget. Praesent ac facilisis arcu, sollicitudin tempus eros.
                Maecenas
                interdum nibh a elit convallis congue. Sed pharetra nibh neque, laoreet consectetur felis auctor
                nec.</p>
            <a href="index.php?page=history" class='page_button'>En savoir plus</a>
        </div>

        <!-- TWITTER FEED -->
        <div class='twitter_feed'>
            <a class="twitter-timeline" href="https://twitter.com/Basketfeminin?ref_src=twsrc%5Etfw">Tweets by
                Basketfeminin</a>
        </div>
    </div>

    <!-- SPONSORS SECTION -->
    <div class="sponsor">
        <h3>Nos sponsors</h3>
        <div class='sponsor_list'>
            <img src="assets/images/logo_ligue_feminine_basketball.png"
                alt="logo de la ligue féminine de basketball française" class='vertical_logo'>
            <img src="assets/images/logo_hyper_u.svg" alt="logo des supermarchés français hyper u" class='horizontal_logo'>
            <img src="assets/images/logo_credit_mutuel.svg" alt="logo de la banque française crédit mutuel" class='horizontal_logo'>
            <img src='assets/images/logo_region_hauts_de_france.svg' alt="logo de la région française Hauts-de-France" class='vertical_logo'>
        </div>
    </div>
</div>