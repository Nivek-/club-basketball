<?php
require_once "autoload.php";

// Instructions qui s'effecuent en fonction du POST qui a été envoyé via l'AJAX
// La première condition permet de faire l'ajout/modification d'un entraînement
if (isset($_POST["action"]) && $_POST["action"] == "add") {

    // Création d'un nouvel objet de la classe Training auquel sont attribuées les propiétés envoyées via le POST
    $training = new Training();
    $training->setDay($_POST["day"]);
    $training->setBegin($_POST["begin"]);
    $training->setEnd($_POST["end"]);
    $training->setRoom($_POST["room"]);
    $training->setCoachId($_POST["coach"]);

    // Si l'id vaut 0 l'entraînement est ajouté à la base de données
    if ($_POST["id"] == 0) {
        $training->addTraining();
    } else {
        //  Sinon l'entraînement est modifé en base de données
        $training->setId($_POST["id"]);
        $training->updateTraining();
    }
    
    // La seconde condition permet la suppression d'un entraînement
} else if (isset($_POST["action"]) && $_POST["action"] == "delete") {
    
    // Création d'un nouvel objet de la classe Training pour le supprimer en base de données
    $training = new Training();
    $training->setId($_POST["id"]);
    $training->deleteTraining();
}

// Importation de tous les entraînements en base de données
$trainings2 = new Training();
$trainings_results = $trainings2->getAll();

// Itération sur chaque élément afin d'afficher les informations de l'entraînement dans une ligne du tableau de la page administration
foreach ($trainings_results as $result) {

    echo "<tr id='" . $result->getId() . "'><td>" . $result->getDay() . "</td>";
    echo "<td class='hide_column'>" . $result->getBegin() . "</td>";
    echo "<td class='hide_column'>" . $result->getEnd() . "</td>";
    echo "<td class='hide_column'>" . $result->getRoom() . "</td>";
    echo "<td>" . $result->getCoachId() . "</td>";

    // Boutons permettant l'édition et la suppression d'un membre
    echo "<td><i class='edit_training fas fa-edit'></i></td>";
    echo "<td><i class='delete_training fas fa-trash'></i></td></tr>";
}
