<?php

if (isset($_POST)) {
    $sender_mail = $_POST["mail"];

    // Vérification de la validité du mail sinon un message d'erreur est envoyé dans la partie front
    if (!filter_var($sender_mail, FILTER_VALIDATE_EMAIL)) {
        echo json_encode(array("message" => "mail"));
    } else {
        // Sinon les données envoyées sont enregistrées dans des variables pour être traitées
        $gender = $_POST["gender"];
        $lastname = $_POST["lastname"];
        $firstname = $_POST["firstname"];
        $message = $_POST["message"];
        $captcha = $_POST["captcha"];

        // Vérification que le captcha n'est pas vide
        // Si c'est le cas un message d'erreur est envoyé dans la partie front
        if ($captcha == "") {
            echo json_encode(array("message" => "null captcha"));

        // Sinon la vérification sur le captcha est faite
        } else {
            // Clé secrète fournie par google pour tester le captcha
            $secret_key = "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe";
            // Envoi de la clé secrète et de la valeur du captcha pour vérifier si c'est correct
            // La réponse est récupérée sous forme de tableau associatif
            $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);

            // S'il y a une erreur, un message est envoyé dans la partie front
            if ($response['success'] == false) {
                echo json_encode(array("message" => "invalid captcha"));
            
            // Sinon l'envoie de mail s'effectue
            } else {
                // Un mail est envoyé au club avec les informations de l'utilisateur et son message
                $club_mail = "testpopschool@gmail.com";
                $subject = "Formulaire de contact transmis par " . $gender . " " . strtoupper($lastname) . " " . $firstname;
                $headers = 'From: ' . $sender_mail . "\r\n" . 'Reply-To: ' . $sender_mail . "\r\n" . 'X-Mailer: PHP/' . phpversion();
                $first_mail = mail($club_mail, $subject, $message, $headers);

                // Un second mail est envoyé au client pour lui confirmer la prise en compte du formulaire
                $sender_subject = "Formulaire de contact transmis au club de basketball d'Avesnes";
                $sender_message = "Message transmis au club: \n" . $message;
                $sender_headers = 'From: ' . $club_mail . "\r\n" . 'Reply-To: ' . $club_mail . "\r\n" . 'X-Mailer: PHP/' . phpversion();
                $secon_mail = mail($sender_mail, $sender_subject, $sender_message, $sender_headers);

                // Si les envois réussissent un message confirme l'envoi dans la partie front
                if ($first_mail && $secon_mail) {
                    echo json_encode(array("message" => "valid"));
                } else {
                    // Sinon un message d'erreur s'affiche dans la partie front
                    echo json_encode(array("message" => "error"));
                }
            }
        }

    }

}
