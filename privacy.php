<div class='container'>
    <!-- TITLE OF THE PAGE -->
    <h3>Politique de confidentialité</h3>

    <!-- PRIVACY POLICY -->
    <div class='legal_content'>
        <h4>A. Introduction</h4>
        <ol>
            <li>La confidentialité des visiteurs de notre site web est très importante à nos yeux, et nous
                nous engageons à la protéger. Cette politique détaille ce que nous faisons de vos informations
                personnelles.</li>
            <li>Consentir à notre utilisation de cookies en accord avec cette politique lors de votre
                première visite de notre site web nous permet d’utiliser des cookies à chaque fois que vous
                consultez notre site.</li>
        </ol>
        <h4>B. Source</h4>
        <p>Ce document a été créé grâce à un modèle de SEQ Legal (<a href="www.seqlegal.com">www.seqlegal.com</a>)</p>
        <p>et a été modifié par Website Planet (<a href="www.websiteplanet.com">www.websiteplanet.com</a>)</p>
        <h4>C. Collecte d’informations personnelles</h4>
        <p>Les types d’informations personnelles suivants peuvent collectés, stockés et utilisés :</p>
        <ol>
            <li>Des informations à propos de votre ordinateur, y compris votre adresse IP, votre localisation
                géographique, le type et la version de votre navigateur, et votre système d’exploitation ;</li>
            <li>Des informations sur vos visites et votre utilisation de ce site web y compris la source
                référente, la durée de la visite, les pages vues, et les chemins de navigation de sites web ;
            </li>

            <li>Des informations générées lors de l’utilisation de notre site, y compris quand, à quelle
                fréquence et sous quelles circonstances vous l’utilisez ;</li>
            <li>Des informations contenues dans toutes les communications que vous nous envoyez par e-mail
                ou sur notre site web, y compris leurs contenus et leurs métadonnées ;</li>
            <li>Toute autre information personnelle que vous nous communiquez.</li>
        </ol>
        <p>Avant de nous divulguer des informations personnelles concernant une autre personne, vous devez
            obtenir le consentement de ladite personne en ce qui concerne la divulgation et le traitement de ces
            informations personnelles selon les termes de cette politique</p>
        <h4>D. Utilisation de vos informations personnelles</h4>
        <p>Les informations personnelles qui nous sont fournies par le biais de notre site web seront utilisées
            dans les objectifs décrits dans cette politique ou dans les pages du site pertinentes. Nous pouvons
            utiliser vos informations personnelles pour:</p>
        <ol>
            <li>Administrer notre site web ;</li>
            <li>Personnaliser notre site web pour vous ;</li>
            <li>Permettre votre utilisation des services proposés sur notre site web ;</li>
            <li>Fournir des informations statistiques à propos de nos utilisateurs à des tierces parties (sans
                que ces tierces parties puissent identifier d’utilisateur individuel avec ces informations) ;
            </li>
            <li>Maintenir la sécurité de notre site web ;</li>
        </ol>

        <h4>E. Divulgation de vos informations personnelles</h4>
        <p>Nous pouvons divulguer vos informations personnelles:</p>
        <ol>
            <li>Dans la mesure où nous sommes tenus de le faire par la loi ;</li>
            <li>Dans le cadre de toute procédure judiciaire en cours ou à venir ;</li>
            <li>À toute personne que nous estimons raisonnablement faire partie intégrante d’un tribunal ou
                autre autorité compétente pour la divulgation de ces informations personnelles si, selon notre
                opinion, un tel tribunal ou une telle autorité serait susceptible de demander la divulgation de
                ces informations personnelles.</li>
        </ol>
        <p>Sauf disposition contraire de la présente politique, nous ne transmettrons pas vos informations
            personnelles à des tierces parties.</p>

        <h4>F. Conservation de vos informations personnelles</h4>
        <ol>
            <li>Cette Section F détaille nos politiques de conservation des données et nos procédures, conçues
                pour nous aider à nous conformer à nos obligations légales concernant la conservation et la
                suppression d’informations personnelles.</li>
            <li>Les informations personnelles que nous traitons à quelque fin que ce soit ne sont pas conservées
                plus longtemps que nécessaire à cette fin ou à ces fins.</li>
            <li>Sans préjudice à l’article F-2, nous supprimerons généralement les données personnelles de ces
                catégories à la date et à l’heure précisées plus bas:</li>
            <ol>
                <li>Ce type de données personnelles sera supprimé {ENTREZ LA DATE/L’HEURE} ; et</li>
                <li>{ENTREZ D’AUTRES DATES/HEURES}.</li>
            </ol>
            <li>Nonobstant les autres dispositions de cette Section F, nous conserverons des documents (y
                compris des documents électroniques) contenant des données personnelles:</li>
            <ol>
                <li>Dans la mesure où nous sommes tenus de le faire par la loi ;</li>
                <li>Si nous pensons que les documents peuvent être pertinents pour toute procédure judiciaire en
                    cours ou potentielle ;</li>
            </ol>
        </ol>
        <h4>G. Sécurité de vos informations personnelles</h4>
        <ol>
            <li>Nous prendrons des précautions techniques et organisationnelles raisonnables pour empêcher la
                perte, l’abus ou l’altération de vos informations personnelle.</li>
            <li>Vous reconnaissez que la transmission d’informations par internet est intrinsèquement non
                sécurisée, et que nous ne pouvons pas garantir la sécurité de vos données envoyées par internet.
            </li>
        </ol>
        <h4>H. Amendements</h4>
        <p>Nous pouvons parfois mettre cette politique à jour en publiant une nouvelle version sur notre site
            web. Vous devez vérifier cette page régulièrement pour vous assurer de prendre connaissance de tout
            changement effectué à cette politique. Nous pouvons vous informer des changements effectués à cette
            politique par courrier électronique ou par le biais du service de messagerie privée de notre site
            web.</p>
        <h4>I. Vos droits</h4>
        <p>Vous pouvez nous demander de vous fournir toute information personnelle que nous détenons sur vous ;
            le transfert de telles informations sera soumis aux conditions suivantes:</p>
        <ol>
            <li>Le règlement de frais ; et</li>
            <li>La présentation de preuves suffisantes de votre identité  (à ces fins, nous acceptons en général une photocopie de votre passeport certifiée
                par notaire plus une copie originale d’une facture de service public indiquant votre adresse
                actuelle).</li>
        </ol>
        <p>Nous pouvons retenir les informations personnelles que vous demandez dans la mesure autorisée par la
            loi.</p>

        <h4>J. Sites web tiers</h4>
        <p>Notre site web contient des liens hypertextes menant vers des sites web tiers et des informations les
            concernant. Nous n’avons aucun contrôle sur ces sites, et ne sommes pas responsables de leurs
            politiques de confidentialité ni de leurs pratiques.</p>

        <h4>K. Cookies</h4>
        <p>Notre site web utilise des cookies. Un cookie est un fichier contenant un identifiant (une chaîne de
            lettres et de chiffres) envoyé par un serveur web vers un navigateur web et stocké par le
            navigateur. L’identifiant est alors renvoyé au serveur à chaque fois que le navigateur demande une
            page au serveur. Les cookies peuvent être « persistants » ou « de session » : un cookie persistant
            est stocké par le navigateur et reste valide jusqu’à sa date d’expiration, à moins d’être supprimé
            par l’utilisateur avant cette date d’expiration ; quant à un cookie de session, il expire à la fin
            de la session utilisateur, lors de la fermeture du navigateur. Les cookies ne contiennent en général
            aucune information permettant d’identifier personnellement un utilisateur, mais les informations
            personnelles que nous stockons à votre sujet peuvent être liées aux informations stockées dans les
            cookies et obtenues par les cookies.Nous n’utilisons que des des cookies de session et des cookies persistants
            sur notre site web.</p>
        <ol>
            <li>Les noms des cookies que nous utilisons sur notre site web et les objectifs dans lesquels nous
                les utilisons sont décrits ci-dessous:</li>
            <ol>
                <li>Nous utilisons Google Analytics et Adwords sur notre site web pour : {INCLUEZ TOUTES LES
                    MANIÈRES DONT LES COOKIES SONT UTILISÉS SUR VOTRE SITE} reconnaître un ordinateur lorsqu’un
                    utilisateur consulte le site web/ suivre les utilisateurs lors de leur navigation sur le
                    site web/ activer l’utilisation d’un panier sur le site web/ améliorer l’utilisation d’un
                    site web/ analyser l’utilisation du site web/ administrer le site web/ empêcher la fraude et
                    améliorer la sécurité du site web/ personnaliser le site web pour chaque utilisateur/
                    envoyer des publicités ciblées pouvant intéresser certains utilisateurs (décrivez vos autres
                    objectifs);</li>
            </ol>
            <li>La plupart des navigateurs vous permettent de refuser ou d’accepter les cookies. Bloquer tous
                les cookies aura un impact négatif sur l’utilisation de nombreux sites web. Si vous bloquez les
                cookies, vous ne pourrez pas utiliser toutes les fonctionnalités de notre site web.</li>
            <li>Vous pouvez supprimer les cookies déjà stockés sur votre ordinateur.</li>
            <li>Supprimer les cookies aura un impact négatif sur l’utilisation de nombreux sites web.</li>
        </ol>
    </div>
</div>
