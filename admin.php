<?php
// Redirection vers la page login si l'utilisateur n'est pas connecté
if(!isset($_SESSION["logged"])) {
header("Location: index.php?page=login");
exit();
}
?>


<div class='container'>
    <!-- TITLE OF THE PAGE -->
    <h3>Administration</h3>
    
    <!-- ADMIN NAVBAR -->
    <ul class='admin_nav'>
        <li class='activated nav_tab' id='articles'>Articles</li>

        <?php 
            // Vérification du role de l'administrateur connecté
            // S'il n'est qu'éditeur alors seuls les articles sont accessibles
            if(isset($_SESSION["logged"])) {
                $logged = $_SESSION["logged"];

                if($logged->getRole() == 0 || $logged->getRole() == 2) {
                    echo "<li class='nav_tab' id='team'>Equipe</li>";
                    echo "<li class='nav_tab' id='trainings'>Entraînements</li>";
                    echo "<li class='nav_tab' id='games'>Matchs</li>";
                    echo "<li class='nav_tab' id='admins'>Administrateurs</li>";
                }
            }
        ?>
        
        <li><a href='index.php?page=disconnect'>Déconnexion</a></li>
    </ul>

    <!-- ADMIN SECTIONS -->
    <div class='admin_sections'>

        <!-- ARTICLES ADMIN SECTION -->
        <div class='section articles section_showed'>
            <!-- ADD ARTICLE  -->
            <a  class='page_button add_article'>Ajouter un article</a>

            <!-- ARTICLES TABLE -->
            <table class='articles_board'>
                <thead>
                    <tr>
                        <td>Titre</td>
                        <td class='hide_column'>Date de rédaction</td>
                        <td>Publication</td>
                        <td colspan="2"></td>
                    </tr>
                </thead>
                <tbody id='articles_table_body'>

                    <?php
                        //  IMPORT TBODY FROM ARTICLE TABLE WITH AJAX INTERACTIONS
                        require_once("article_table.php");
                    ?>

                </tbody>
            </table></div>

        <!-- TEAM ADMIN SECTION -->
        <div class='section team'>
            
            <!-- ADD MEMBER  -->
             <a  class='page_button add_staff'>Ajouter un membre</a>

             <!-- TEAM TABLE -->
            <table class='team_players_board'>
                <thead>
                    <tr>
                        <td>Nom</td>
                        <td class='hide_column'>Prénom</td>
                        <td>Rôle</td>
                        <td class='hide_column'>Poste</td>
                        <td colspan="2"></td>
                    </tr>
                </thead>
                <tbody id='staff_table_body'>

                    <?php
                        //  IMPORT TBODY FROM MEMBER ACTIONS WITH AJAX INTERACTIONS
                        require_once("member_actions.php");
                    ?>

                </tbody>
            </table>
        </div>

        <!-- TRAININGS ADMIN SECTION -->
        <div class='section trainings'>
            <!-- ADD TRAINING  -->
             <a  class='page_button add_training'>Ajouter un entraînement</a>

             <!-- TRAININGS TABLE -->
            <table class='trainings_board'>
                <thead>
                    <tr>
                        <td>Jour</td>
                        <td class='hide_column'>Heure de début</td>
                        <td class='hide_column'>Heure de fin</td>
                        <td class='hide_column'>Salle</td>
                        <td>Coach</td>
                        <td colspan="2"></td>
                    </tr>
                </thead>
                <tbody id='trainings_table_body'>

                    <?php
                        //  IMPORT TBODY FROM TRAINING ACTIONS WITH AJAX INTERACTIONS
                        require_once("trainings_actions.php");
                    ?>

                </tbody>
            </table>
        </div>

        <!-- GAMES ADMIN SECTION -->
        <div class='section games'>
            <!-- ADD GAME -->
            <a  class='page_button add_game'>Ajouter un match</a>

            <!-- GAMES TABLE -->
            <table class='games_board'>
                <thead>
                    <tr>
                        <td>Date</td>
                        <td class='hide_column'>Heure</td>
                        <td>Adversaire</td>
                        <td colspan="2"></td>
                    </tr>
                </thead>
                <tbody id='games_table_body'>

                <?php
                    //  IMPORT TBODY FROM GAMES ACTIONS WITH AJAX INTERACTIONS
                    require_once("games_actions.php");
                ?>

                 </tbody>
            </table>
        </div>

        <!-- ADMINISTRATORS ADMIN SECTION -->
        <div class='section admins'>
             <!-- ADD ADMIN  -->
             <a  class='page_button add_admin'>Ajouter un administrateur</a>

             <!-- ADMIN TABLE -->
            <table class='admins_board'>
                <thead>
                    <tr>
                        <td>Nom d'utilisateur</td>
                        <td>Rôle</td>
                        <td colspan="2"></td>
                    </tr>
                </thead>
                <tbody id='admin_table_body'>

                    <?php
                        //  IMPORT TBODY FROM ADMIN_TABME WITH AJAX INTERACTIONS
                        require_once("admin_table.php");
                    ?>

                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- ARTICLES MODAL FETCH FROM BOOTSTRAP AND MODIFIED -->
<div class="modal" id="addArticleModal" tabindex="-1" role="dialog" aria-labelledby="addArticleModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addArticleModalTitle"></h5>
      </div>
      <div class="modal-body">

          <!-- FORM TO ADD/EDIT ARTICLE -->
          <form id='article_form' method="POST" enctype="multipart/form-data">
            <div class="form_input">
                <label for="title">Titre</label>
                <input type="text" name=title id="title" />
            </div>
            <div class="form_input">
                <label for="content">Contenu</label>
                <textarea id="content" name="content"></textarea>
            </div>
            <div class='form_input'>
                <label for="image">Image (max 500Ko, formats autorisés: .png, .jpg, .jpeg)</label>
                <input type="file" id="image" name="image" accept="image/png, image/jpeg, image/jpg">
            </div>
            <?php 
                echo "<input type='hidden' name='article_date' id='article_date' value='".time()."'>";
                // Vérification du rôle de l'admin connecté
                // S'il n'est qu'éditeur, il n'a pas accès à l'input pour publier l'article
                // L'input est seulement caché pour conserver lors valeur enregistrée s'il s'agit d'une modification
                if(isset($_SESSION["logged"])) {
                    if($logged->getRole() == 0 || $logged->getRole() == 2) {
                        echo "<div class='form_input publish_checkbox'>";
                    } else {
                        echo "<div class='form_input publish_checkbox hide_publish'>";
                    }
                }

                
            ?>

                <input type='checkbox' id='publish' name='publish'>
                <label for='publish'>Publié</label>
            </div>
            <input type="hidden" name="article_id" id="article_id" value="0" />
          </form>
          <p class='click_message'></p>
      </div>
      <div class="modal-footer">
        <!-- CONFIRM AND CLOSE MODAL BUTTONS -->
        <input type="submit" form="article_form" class="page_button article_valid_button"></button>
        <button type="button" class="page_button" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>


<!-- TEAM MODAL FETCH FROM BOOTSTRAP AND MODIFIED -->
<div class="modal" id="addStaffModal" tabindex="-1" role="dialog" aria-labelledby="addStaffModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addStaffModalTitle"></h5>
      </div>
      <div class="modal-body">

          <!-- FORM TO ADD/EDIT STAFF MEMBER -->
          <form>
            <div class="form_input">
                <label for="lastname">Nom</label>
                <input type="text" name="lastname" id="lastname" />
            </div>
            <div class="form_input">
                <label for="firstname">Prénom</label>
                <input type="text" name="firstname" id="firstname" />
            </div>
            <div class="form_input" id='role_input_block'>
                <label for="role">Role</label>
                <select name="role" id='role'>
                    <option value="0">Joueuse</option>
                    <option value="1">Coach</option>
                </select>
            </div>
            <div class="form_input" id='position_input_block'>
                <label for="position">Poste</label>
                <select name="position" id='position'>
                    <option  value="point_guard">Meneur</option>
                    <option value="shooting_guard">Arrière</option>
                    <option value="small_forward">Ailier</option>
                    <option value="power_forward">Ailier fort</option>
                    <option value="center">Pivot</option>
                </select>
            </div>
            <input type="hidden" name="member_id" id="member_id" value="0" />
          </form>
          <p class='click_message'></p>
      </div>
      <div class="modal-footer">
        <!-- CONFIRM AND CLOSE MODAL BUTTONS -->
        <button type="button"  class="page_button staff_valid_button"></button>
        <button type="button" class="page_button" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>


<!-- TRAININGS MODAL FETCH FROM BOOTSTRAP AND MODIFIED -->
<div class="modal" id="addTrainingModal" tabindex="-1" role="dialog" aria-labelledby="addTrainingModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addTrainingModalTitle"></h5>
      </div>
      <div class="modal-body">

          <!-- FORM TO ADD/EDIT TRAINING -->
          <form>
            <div class="form_input">
                <label for="day">Jour</label>
                <select name="day" id='day'>
                    <option selected value="lundi">Lundi</option>
                    <option value="mardi">Mardi</option>
                    <option value="mercredi">Mercredi</option>
                    <option value="jeudi">Jeudi</option>
                    <option value="vendredi">Vendredi</option>
                    <option value="samedi">Samedi</option>
                    <option value="dimanche">Dimanche</option>
                </select>
            </div>
            <div class="form_input">
                <label for="begin">Heure de début</label>
                <input type="time" name="begin" id="begin">
            </div>
            <div class="form_input">
                <label for="end">Heure de fin</label>
                <input type="time" name="end" id="end">
            </div>
            <div class="form_input">
                <label for="room">Salle</label>
                <input type="text" name="room" id="room">
            </div>
            
            <div class="form_input">
                <label for="coach">Coach</label>
                <select name='coach' id='coach'>
                <?php
                    $staff = new Staff();
                    $staff_results = $staff->getAll();
                    $i = 0;
                    foreach ($staff_results as $result) {

                        if($result->getRole() == 1) {
                            if($i == 0) {
                                echo "<option selected value='".$result->getId()."'>".$result->getFirstname()." ".$result->getLastname()."</option>";
                            } else {
                                echo "<option value='".$result->getId()."'>".$result->getFirstname()." ".$result->getLastname()."</option>";
                            }
                            $i++;
                        }
                    }
            ?>
                </select>
            </div>
            <input type="hidden" name="training_id" id="training_id" value="0" />
          </form>
          <p class='click_message'></p>
      </div>
      <div class="modal-footer">
        <!-- CONFIRM AND CLOSE MODAL BUTTONS -->
        <button type="button"  class="page_button training_valid_button"></button>
        <button type="button" class="page_button" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>


<!-- GAMES MODAL FETCH FROM BOOTSTRAP AND MODIFIED -->
<div class="modal" id="addGameModal" tabindex="-1" role="dialog" aria-labelledby="addGameModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addGameModalTitle"></h5>
      </div>
      <div class="modal-body">

          <!-- FORM TO ADD/EDIT Game -->
          <form>
            <div class="form_input">
                <label for="date">Date</label>
                <?php
                $date = getDate();
                
               $todays_date = date("Y-m-d", time($date));

               echo "<input type='date' id='date' name='date'min='".$todays_date."' max='2100-12-31'>";
            ?>
            </div>
            <div class="form_input">
                <label for="hour">Heure</label>
                <input type="time" name="hour" id="hour">
            </div>
            <div class="form_input">
                <label for="opponent">Adversaire</label>
                <input type="text" name="opponent" id="opponent">
            </div>
            <input type="hidden" name="game_id" id="game_id" value="0" />
          </form>
          <p class='click_message'></p>
      </div>
      <div class="modal-footer">
        <!-- CONFIRM AND CLOSE MODAL BUTTONS -->
        <button type="button"  class="page_button game_valid_button"></button>
        <button type="button" class="page_button" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>


<!-- ADMINS MODAL FETCH FROM BOOTSTRAP AND MODIFIED -->
<div class="modal" id="addAdminModal" tabindex="-1" role="dialog" aria-labelledby="addAdminModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addAdminModalTitle"></h5>
      </div>
      <div class="modal-body">

          <!-- FORM TO ADD/EDIT ADMIN -->
          <form>
            <div class="form_input">
                <label for="username">Nom d'utilisateur</label>
                <input type="text" name="username" id="username" />
            </div>
            <div class="form_input pass_input_block">
                <label for="password">Mot de passe</label>
                <input type="password" name="password" id="password" />
            </div>
            <div class="form_input pass_input_block">
                <label for="confirm_password">Confimer mot de passe</label>
                <input type="password" name="confirm_password" id="confirm_password" />
            </div>
            <div class="form_input">
                <label for="admin_role">Role</label>
                <select name="admin_role" id='admin_role'>
                    <option value="0">Administrateur</option>
                    <option value="1">Editeur</option>
                </select>
            </div>
            <input type="hidden" name="admin_id" id="admin_id" value="0" />
          </form>
          <p class='click_message'></p>
      </div>
      <div class="modal-footer">
        <!-- CONFIRM AND CLOSE MODAL BUTTONS -->
        <button type="button"  class="page_button admin_valid_button"></button>
        <button type="button" class="page_button" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>