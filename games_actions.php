<?php
require_once "autoload.php";

// Instructions qui s'effecuent en fonction du POST qui a été envoyé via l'AJAX
// La première condition permet de faire l'ajout/modification d'un match
if (isset($_POST["action"]) && $_POST["action"] == "add") {

    // Création d'un nouvel objet de la classe Game auquel sont attribuées les propiétés envoyées via le POST
    $game = new Game();
    $game->setDateGame(strtotime($_POST["date"]));
    $game->setHour($_POST["hour"]);
    $game->setOpponent($_POST["opponent"]);

    // Si l'id vaut 0 le match est ajouté à la base de données
    if ($_POST["id"] == 0) {
        $game->addGame();
    } else {
        //  Sinon le match est modifé en base de données
        $game->setId($_POST["id"]);
        $game->updateGame();
    }
    
    // La seconde condition permet la suppression d'un match
} else if (isset($_POST["action"]) && $_POST["action"] == "delete") {
    
    // Création d'un nouvel objet de la classe Game pour le supprimer en base de données
    $game = new Game();
    $game->setId($_POST["id"]);
    $game->deleteGame();
}

// Importation de tous les matchs en base de données
$game2 = new Game();
$game_results = $game2->getAll();

// Itération sur chaque élément afin d'afficher les informations du match dans une ligne du tableau de la page administration
foreach ($game_results as $result) {
    $game_date = date("d/m/Y", $result->getDateGame());

    
    echo "<tr id='" . $result->getId() . "'><td>" . $game_date . "</td>";
    echo "<td class='hide_column'>" . $result->getHour() . "</td>";
    echo "<td>" . $result->getOpponent() . "</td>";

    // Boutons permettant l'édition et la suppression d'un membre
    echo "<td><i class='edit_game fas fa-edit'></i></td>";
    echo "<td><i class='delete_game fas fa-trash'></i></td></tr>";
}
