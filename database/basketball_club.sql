-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: basketball_club
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (16,'admin','$2y$10$f70TeSQVJSgngItELCH3h.FQbc1dTIsQ8.qGbMQJtObGOpL8S8PpG',2),(17,'clubavesnes','$2y$10$lgkDHvqMQGY56Bw7vi1HSeREWxZqQxbdeCs8ScIGwm8mrTaZnbx9.',0),(18,'editeur','$2y$10$olMSAoBXUNchhJ1ewMflqOKVY0t1QaYq5.qnBPrkiyyRyYARdpsW6',1);
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `content` mediumtext COLLATE utf8_bin NOT NULL,
  `date` bigint(20) NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (20,'Lorem ipsum dolor sit amet, consectetur adipiscing elit','<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify; background-color: #ffffff;\"><strong>Suspendisse</strong> quis erat ut magna dictum bibendum sit amet id ligula. Proin in massa nulla. Integer vel nulla vel risus luctus maximus vitae at felis. Proin commodo luctus metus eget luctus. Ut faucibus vitae neque eleifend malesuada. Maecenas iaculis varius mi, vitae interdum neque sagittis nec. Vivamus aliquet convallis mi, eget facilisis urna pharetra at. Fusce et ante metus. </span></p>\n<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify; background-color: #ffffff;\">Maecenas id pulvinar lectus. Praesent ullamcorper sed ex ut sollicitudin. Aliquam a ligula nec est blandit fermentum. Nullam sodales est et mauris pretium, ac dignissim lorem ultricies. Aliquam urna nunc, accumsan a augue in, eleifend pharetra ante.</span></p>',1588858619,1,'feminine_basketball_game.jpg'),(21,'Sed vitae odio interdum','<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify; background-color: #ffffff;\">Nunc turpis mi, cursus ac metus sed, malesuada viverra mi. Sed a hendrerit est, eget tempus quam. Praesent interdum vehicula augue tincidunt mattis. Phasellus dui urna, iaculis vitae luctus ac, iaculis id turpis. Duis ornare quam nec erat scelerisque, in aliquet nisi luctus. Vestibulum sit amet pretium enim. In a tristique tortor, eget mattis libero. Aliquam volutpat eu sem et imperdiet. Sed interdum hendrerit velit.</span></p>\n<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify; background-color: #ffffff;\"><strong>Etiam </strong>auctor justo sit amet odio varius vestibulum.</span></p>',1588856592,1,'basketball.jpg'),(22,'Donec eu massa eu turpis varius dictum','<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify; background-color: #ffffff;\">Pellentesque rutrum est massa, eu mattis sapien mollis vel. Curabitur dictum nisi nibh, faucibus cursus ipsum venenatis vitae. Aliquam ut mi id dui sagittis dignissim. In quis metus ornare, dignissim eros nec, convallis purus. Nam sit amet dictum ex, in semper odio. Sed sagittis pulvinar sem, ut facilisis arcu ultrices a. Curabitur <strong>non laoreet</strong> ligula, non mattis mauris. Nunc ut facilisis nisl. Duis mollis egestas nulla nec mattis. Nulla sapien turpis, gravida non nisl at, volutpat bibendum est. Sed pharetra id massa aliquet porttitor. Nam eu accumsan nunc.</span></p>\n<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify; background-color: #ffffff;\">Donec sed vehicula nunc. In hac<em> habitasse platea </em>dictumst. Integer commodo quam ac tellus auctor tincidunt. Sed porttitor eros ut leo gravida dictum. Duis ultricies est ac erat fringilla rutrum. Cras ac eros tincidunt tellus aliquam blandit et ac purus. Nulla sagittis semper sollicitudin.</span></p>\n<p><span style=\"background-color: #ffffff; font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;\">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. <strong>Sed vel</strong> iaculis risus. Donec consectetur ultrices bibendum. Sed eget enim a velit fringilla viverra non sed lectus. Curabitur bibendum tincidunt risus et fringilla. Maecenas volutpat maximus elit, eu egestas ligula consectetur ac. Ut vel euismod <strong>diam</strong>.</span></p>',1588858782,1,'femine_basketball_team_game_beginning.jpg');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` bigint(20) NOT NULL,
  `hour` varchar(255) COLLATE utf8_bin NOT NULL,
  `opponent` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES (10,1588543200,'20:00','Lyon'),(11,1652565600,'20:00','Lille'),(12,1611010800,'18:00','Nimes'),(13,1592604000,'20:00','Marseille');
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(255) COLLATE utf8_bin NOT NULL,
  `firstname` varchar(255) COLLATE utf8_bin NOT NULL,
  `role` tinyint(1) NOT NULL,
  `position` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` VALUES (1,'GAUVRIT','Marylou',0,'point_guard'),(2,'MOLLE','Charline',0,'point_guard'),(3,'LE BALCH','Marion',0,'shooting_guard'),(4,'KUPELIAN','Joanna',0,'shooting_guard'),(5,'BONHOMMEAU','Morgane',0,'small_forward'),(6,'ACHARD','Corinne',0,'small_forward'),(7,'GOUY','Adeline',0,'power_forward'),(8,'BILLON','AurÃ©lie',0,'power_forward'),(9,'NICOLEAU','MaÃ©va',0,'center'),(10,'RELET','Miguel',1,NULL),(11,'RAINGEARD','BenoÃ®t',1,NULL),(12,'GOUY','StÃ©phane',1,NULL),(13,'PROUX','Alexis',1,NULL);
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trainings`
--

DROP TABLE IF EXISTS `trainings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trainings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` varchar(255) COLLATE utf8_bin NOT NULL,
  `begin` varchar(255) COLLATE utf8_bin NOT NULL,
  `end` varchar(255) COLLATE utf8_bin NOT NULL,
  `room` varchar(255) COLLATE utf8_bin NOT NULL,
  `coach_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trainings`
--

LOCK TABLES `trainings` WRITE;
/*!40000 ALTER TABLE `trainings` DISABLE KEYS */;
INSERT INTO `trainings` VALUES (1,'mercredi','19:00','20:30','vrignaud b',11),(2,'vendredi','19:00','20:30','vrignaud b',10);
/*!40000 ALTER TABLE `trainings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-08 18:29:38
