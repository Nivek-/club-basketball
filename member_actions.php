<?php
require_once "autoload.php";

// Instructions qui s'effecuent en fonction du POST qui a été envoyé via l'AJAX
// La première condition permet de faire l'ajout/modification d'un membre
if (isset($_POST["action"]) && $_POST["action"] == "add") {

    // Création d'un nouvel objet de la classe Staff auquel sont attribuées les propiétés envoyées via le POST
    $staff = new Staff();
    $staff->setLastname($_POST["lastname"]);
    $staff->setFirstname($_POST["firstname"]);
    $staff->setRole($_POST["role"]);
    $staff->setPosition($_POST["position"]);

    // Si l'id vaut 0 le membre est ajouté à la base de données
    if ($_POST["id"] == 0) {
        $staff->addStaff();
    } else {
        //  Sinon le membre est modifé en base de données
        $staff->setId($_POST["id"]);
        $staff->updateStaff();
    }
    
    // La seconde condition permet la suppression d'un membre
} else if (isset($_POST["action"]) && $_POST["action"] == "delete") {
    
    // Création d'un nouvel objet de la classe Staff pour le supprimer en base de données
    $staff = new Staff();
    $staff->setId($_POST["id"]);
    $staff->deleteStaff();
}

// Importation de tous les membres de l'équipe en base de données
$staff2 = new Staff();
$staff_results = $staff2->getAll();

// Itération sur chaque élément afin d'afficher les informations du membre de l'équipe dans une ligne du tableau de la page administration
foreach ($staff_results as $result) {
    //Récupération du poste des joueuses traduit en français
    $result->translatePosition();

    echo "<tr id='" . $result->getId() . "'><td>" . $result->getLastname() . "</td>";
    echo "<td class='hide_column'>" . $result->getFirstname() . "</td>";
    echo "<td>";
    echo ($result->getRole() == 0) ? "Joueuse" : "Coach";
    echo "</td>";
    echo "<td class='hide_column'>";
    echo ($result->getRole() == 0) ? ucfirst($result->translatedPosition) : "-";
    echo "</td>";

    // Boutons permettant l'édition et la suppression d'un membre
    echo "<td><i class='edit_staff fas fa-edit'></i></td>";
    echo "<td><i class='delete_staff fas fa-trash'></i></td></tr>";
}
