<?php
require_once "autoload.php";

// Instructions qui s'effecuent en fonction du POST qui a été envoyé via l'AJAX
// La première condition permet de faire l'ajout/modification d'un article
if (isset($_POST["action"]) && $_POST["action"] == "add") {

    // Création d'un nouvel objet de la classe Article auquel sont attribuées les propiétés envoyées via le POST
    $article = new Article();
    $article->setTitle($_POST["title"]);
    $article->setArticleDate($_POST["article_date"]);
    $article->setContent($_POST["content"]);
    $article->setPublish($_POST["publish"]);

    // Si une image a été envoyée, son nom et son poids son récupérés dans des variables
    if (isset($_FILES["image"]) && $_FILES["image"] != null) {
        $filename = $_FILES["image"]["name"];
        $filesize = $_FILES["image"]["size"];
        $article->setImage($filename);
    }

    $error = null;
    $ext;

    // Si l'id vaut 0 l'article' est ajouté à la base de données après vérification de l'image
    if ($_POST["id"] == 0) {
        check_image($error, $ext, $filename, $filesize);
        if ($error == null) {
            $article->addArticle();
            refresh_article_table();
        }
        //  Sinon l'article est modifé en base de données 
    } else {
        $article->setId($_POST["id"]);
        // Si une image a été envoyée et qu'elle est conforme, elle est également modifiée en BDD
        if (isset($filename) && $filename != "") {
            check_image($error, $ext, $filename, $filesize);
            if ($error == null) {
                $article->updateArticle();
                $article->updateImage();
                refresh_article_table();
            }
        } else {
            $article->updateArticle();
            refresh_article_table();
        }
    }

    

    // La seconde condition permet la suppression d'un article
} else if (isset($_POST["action"]) && $_POST["action"] == "delete") {

    // Création d'un nouvel objet de la classe Article pour le supprimer en base de données
    $article = new Article();
    $article->setId($_POST["id"]);
    $article->getArticle($article->getId());
    $article->deleteArticle();

    // Suppression de l'image correspondante dans le dossier
    unlink("./assets/upload/" . $article->getImage());
    refresh_article_table();
}

// Fonction permettant d'actualiser le tableau contenant les articles
function refresh_article_table()
{
    require_once "article_table.php";
}

// Fonction permettant de vérifier le poids et l'extension de l'image
// Elle renvoit des erreurs au format JSON si l'un ou l'autre n'est pas bon
function check_image(&$error, &$ext, $filename, $filesize)
{
    // Tableau contenant les extensions autorisées
    $allowed = array("jpg", 'jpeg', "png");

    // Vérifie l'extension du fichier
    $ext = pathinfo($filename, PATHINFO_EXTENSION);

    // Vérifie la taille du fichier - 500Ko maximum
    $maxsize = 500 * 1024;

    // Si des erreurs sont trouvées, elles sont envoyées au format JSON
    // Sinon l'image est enregistrée dans le dossier
    if ($filesize > $maxsize) {
        $error = "size";
        echo json_encode(array('message' => $error));
    } else if (!in_array($ext, $allowed)) {
        $error = "extension";
        echo json_encode(array('message' => $error));
    } else {
        $error = null;
        move_uploaded_file($_FILES["image"]["tmp_name"], "assets/upload/" . $_FILES["image"]["name"]);
    }
}
