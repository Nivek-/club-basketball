<?php
require_once "autoload.php";

// Importation de tous les admins en base de données
$admin2 = new Admin();
$admin_results = $admin2->getAll();

// Itération sur chaque élément afin d'afficher les informations de l'admin dans une ligne du tableau de la page administration
foreach ($admin_results as $result) {

    echo "<tr id='" . $result->getId() . "'><td>" . $result->getUsername() . "</td>";
    echo "<td>";
    echo ($result->getRole() == 0) ? "Administrateur" : "Editeur";
    echo "</td>";

    // Boutons permettant l'édition et la suppression d'un admin
    echo "<td><i class='edit_admin fas fa-edit'></i></td>";
    echo "<td><i class='delete_admin fas fa-trash'></i></td></tr>";
}

?>