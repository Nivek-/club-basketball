<!-- TITLE OF THE PAGE -->
<h3>L'histoire du basketball féminin</h3>

<!-- TIMELINE -->
<div class="timeline">

    <!-- LEFT CARD -->
    <div class="card left">
        <div class="card_content from_left">
            <div class='card_image'>
                <img src="assets/images/basketball_hoop.jpg" alt="panier de basketball d'une salle de sport" class='hoop'>
            </div>
            <div class='card_text'>
                <h4>Lorem ipsum dolor</h4>
                <p>Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto mnesarchum, vim ea
                    mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua dignissim per, habeo iusto
                    primis ea eam.</p>
            </div>
        </div>
    </div>
    
    <!-- RIGHT CARD -->
    <div class="card right">
        <div class="card_content from_right">
            <div class='card_image'>
                <img src="assets/images/feminine_basketball_team_coach_talking_players.jpg" alt="Coach d'une équipe de basketball féminine parlant à ses joueuses">
            </div>
            <div class='card_text'>
                <h4>Mauris vel leo dictum, pretium arcu quis</h4>
                <p>Aliquam aliquam, turpis in pulvinar efficitur, nibh est egestas sapien, ac pellentesque lacus mi eu
                    sapien. Aliquam porttitor tortor in dapibus viverra. Sed nec tellus feugiat, hendrerit sapien vel,
                    volutpat dui. Aliquam tempus vestibulum diam, vel tincidunt mauris rutrum vitae. Aliquam finibus
                    nisl quis metus tristique, nec lacinia quam vulputate. Curabitur mollis consequat ligula, a euismod
                    nibh lacinia ut.</p>
            </div>
        </div>
    </div>

    <!-- LEFT CARD -->
    <div class="card left">
        <div class="card_content from_left">
            <div class='card_image high'>
                <img src="assets/images/feminine_basketball_player_shooting.jpg" alt="Joueuse de basketball portant la tenue de son équipe en train de lancer le ballon vers le panier">
            </div>
            <div class='card_text'>
                <h4>Phasellus volutpat aliquet est eget cursus</h4>
                <p>Phasellus eu sodales eros, mollis gravida odio. Cras eu commodo odio. Sed sapien ipsum, vehicula a
                    magna vel, auctor eleifend dui. Quisque porttitor velit ac enim euismod mattis. Vestibulum cursus
                    sapien vitae libero iaculis vestibulum. Praesent at cursus tellus. Nam ut mi facilisis, rutrum nunc
                    ut, ullamcorper tortor.</p>
            </div>
        </div>
    </div>

    <!-- RIGHT CARD -->
    <div class="card right">
        <div class="card_content from_right">
            <div class='card_image'>
                <img src="assets/images/basktball_on_field_with_instructions_book.jpg" alt="Ballon de basketball sur un terrain de salle de sport à côté d'un manuel d'instructions pour s'entraîner">
            </div>
            <div class='card_text'>
                <h4>Nunc ornare tempus dui auctor porta</h4>
                <p>Cras massa tortor, rutrum vitae lobortis venenatis, ornare id mi. Ut sed feugiat mauris. Proin
                    vehicula non purus et cursus. Maecenas volutpat pretium convallis. Vestibulum ante ipsum primis in
                    faucibus orci luctus et ultrices posuere cubilia curae; Praesent dictum molestie urna ut gravida</p>
            </div>
        </div>
    </div>
</div>