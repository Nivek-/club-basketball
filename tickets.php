<div class="container">
    <!-- TITLE OF THE PAGE AND INFORMATIONS ABOUT TICKETS -->

    <h3>Les tarifs d'un match</h3>
    <div class="tickets_informations">
        <p>Calculez en quelques clics le prix de revient pour assister à nos matchs</p>
        <p>Une réduction de 2€ est prévue pour <strong>les femmes</strong>
            et <strong>les filles</strong> afin de soutenir nos joueuses</p>
    </div>
</div>

<!-- CALCULATOR SECTION -->
<div class='covered_section covered_calculator'>
    <img src='assets/images/womens_basketball_team_grouping.jpg' alt='équipe féminine de basketball regroupé en cercle'>

    <!-- CALCULATOR BLOCK -->
    <div class='calculator'>
        <h4>Tarifs</h4>
        <div class="prices_list">

            <!-- KIDS UNDER 5 YEARS PRICE -->
            <ul>
                <li>Moins de 5 ans</li>
                <li class='free_ticket'></li>
                <li class='price'>Gratuit</li>
            </ul>

            <!-- KIDS BETWEEN 5 AND 10 YEARS OLD PRICE -->
            <ul>
                <li>Enfants de 5 à 10 ans</li>
                <li class='tickets_number'>
                    <i class="fas fa-minus change_value inactive"></i>
                    <input class='price_input lowprice' type="number" name='lowprice' id='950' value="0" min="0"
                        readonly />
                    <i class="fas fa-plus change_value"></i>
                </li>
                <li class='price'>9,50 €</li>
            </ul>

            <!-- KIDS OLDER THAN 10 YEARS PRICE -->
            <ul>
                <li>Plus de 10 ans</li>
                <li class='tickets_number'>
                    <i class="fas fa-minus change_value inactive"></i>
                    <input class='price_input' type="number" name='middleprice' id='1150' value="0" min="0" readonly />
                    <i class="fas fa-plus change_value"></i>
                </li>
                <li class='price'>11,50 €</li>

            </ul>

            <!-- ADULTS PRICE -->
            <ul>
                <li>Adultes (plus de 18 ans)</li>
                <li class='tickets_number'>
                    <i class="fas fa-minus change_value inactive"></i>
                    <input class='price_input' type="number" name='fullprice' id='1300' value="0" min="0" step="1"
                        readonly />
                    <i class="fas fa-plus change_value"></i>
                </li>
                <li class='price'>13,00 €</li>
            </ul>

            <!-- DISCOUNT PART -->
            <p>Sélectionner le nombre de femmes/filles de 5 ans et plus présentes pour calculer le montant avec la
                réduction
            </p>

            <!-- DISCOUNT SELECTOR -->
            <ul class='discount'>
                <li>Femmes/Filles</li>
                <li class='tickets_number'>
                    <i class="fas fa-minus change_value discount_minus inactive"></i>
                    <input class='price_input discount_number' type="number" name='fullprice' id='200' value="0" min="0" step="1"
                        readonly />
                    <i class="fas fa-plus change_value discount_plus inactive"></i>
                </li>
                <li>- 2,00 €</li>
            </ul>
        </div>
        <div class="line"></div>

        <!-- TOTAL AMOUNT -->
        <div class='amount_bloc'>
            <p>Total</p>
            <p><span id='amount'></span> €</p>
        </div>
    </div>
    <!-- END CALCULATOR BLOCK -->

</div>
<!-- END CALCULATOR SECTION-->

<!-- OTHERS INFORMATIONS ABOUT TICKETS -->
<div class="container">
    <div class="tickets_informations">
        <p>Les places sont à acheter directement à l’accueil de notre salle</p>
        <p>Pour tout renseignement, n’hésitez pas à nous <a href="index.php?page=contact">contacter</a> ! </p>
    </div>
</div>