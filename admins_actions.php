<?php
require_once "autoload.php";

// Instructions qui s'effecuent en fonction du POST qui a été envoyé via l'AJAX
// La première condition permet de faire l'ajout/modification d'un admin
if (isset($_POST["action"]) && $_POST["action"] == "add") {

    // Création d'un nouvel objet de la classe Admin auquel sont attribuées les propiétés envoyées via le POST
    $admin = new Admin();
    $admin->setUsername($_POST["username"]);
    $admin->setRole($_POST["adminRole"]);

    $valid;
    // Si l'id vaut 0 l'admin est ajouté à la base de données
    if ($_POST["id"] == 0) {
        // Cryptage du mot de passe entré
        $hashed_pass = password_hash($_POST["password"], PASSWORD_DEFAULT);
        $admin->setPassword($hashed_pass);
        
        $admin->addAdmin($valid);
        
    } else {
        //  Sinon l'admin est modifé en base de données
        $admin->setId($_POST["id"]);
        $admin->updateAdmin($valid);
    }

    // Si le paramètre renvoyé par la fonction addAdmin a la valeur username
    if($valid == "username") {
        // Un message est envoyé au format JSON dans la partie front afin d'afficher une erreur indiquant que l'utilisateur existe déjà 
        echo json_encode(array('message' => $valid));
    } else {
        // Sinon c'est le tableau contenant le nouvel élément qui est envoyé 
        refresh_admin_table();
    }
    
    // La seconde condition permet la suppression d'un admin
} else if (isset($_POST["action"]) && $_POST["action"] == "delete") {
    
    // Création d'un nouvel objet de la classe Admin pour le supprimer en base de données
    $admin = new Admin();
    $admin->setId($_POST["id"]);
    $admin->deleteAdmin();
    refresh_admin_table();
}

// Fonction permettant d'actualiser le tableau contenant les admins
function refresh_admin_table() {
    require_once("admin_table.php");
}