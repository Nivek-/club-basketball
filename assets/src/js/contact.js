// Attends que la page soit chargée avant d'éxécuter le script
$(document).ready(function () {

    // Vérifie que la page est bien 'contact'
    if (page == "contact") {
        $("#contact_form").on("submit", function (event) {
            //on empeche le formulaire de recharger la page
            event.preventDefault();

            // Récupération des valeurs des inputs du formulaire
            var gender = $('input[name=gender]:checked').val()
            var lastname = $("#lastname").val()
            var firstname = $("#firstname").val();
            var mail = $("#mail").val()
            var message = $("#message").val()
            var agree = $("#agree:checked").val()
            var captcha = grecaptcha.getResponse()

            // Vérification que les champs ne soient pas vides et que les cases soient cochées
            // Si ce n'est pas le cas un message indique à l'utilisateur de le faire
            if (gender == undefined || lastname == "" || firstname == "" || mail == "" || message == "" || agree == undefined) {
                $('.send_message').html("Veuillez remplir tous les champs et cocher les cases pour envoyer le formulaire")
            } else {
                // Sinon les valeurs sont enregistrées dans un FormData pour être envoyées en post
                var formData = new FormData()
                formData.append('gender', gender);
                formData.append('lastname', lastname);
                formData.append('firstname', firstname);
                formData.append('mail', mail);
                formData.append('message', message);
                formData.append('captcha', captcha)

                // Envoi en POST des informations saisies dans le formulaire
                $.ajax({
                    url: 'send_mail.php',
                    type: 'POST',
                    data: formData,
                    dataType: 'JSON',
                    processData: false,
                    contentType: false,
                    success: function (callback) {
                        // En fonction du résultat envoyé par le traitement en back un message est adressé à l'utilisateur
                        if (callback.message == "mail") {
                            $('.send_message').html("Veuillez entrer une adresse mail valide")
                        } else if (callback.message == "error") {
                            $('.send_message').html("Erreur de l'envoi du mail, veuillez réessayer")
                        } else if (callback.message == "null captcha") {
                            $('.send_message').html("Veuillez valider le captcha")
                        } else if (callback.message == "invalid captcha") {
                            $('.send_message').html("Captcha incorrect")
                        } else {

                            // Remise à zéro des valeurs du formulaire lorsque l'envoi du mail fonctionne
                            $('input[name=gender]').prop("checked", false)
                            $("#lastname").val("")
                            $("#firstname").val("");
                            $("#mail").val("")
                            $("#message").val("")
                            $("#agree").prop("checked", false)
                            grecaptcha.reset();
                            
                            $('.send_message').html("Mail envoyé !")
                        }
                    }
                });
            }
        })
    }
})