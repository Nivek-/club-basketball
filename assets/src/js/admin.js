$(document).ready(function () {
    if (page == "admin") {

        var id, action, object, activePass, sendImage;

        // NAVIGATION DE LA PARTIE ADMIN

        // Evénement au clic sur un bouton du menu admin
        $(".nav_tab").click(function () {

            // Retrait de la classe activated à tous les onglets du menu
            $(".nav_tab").removeClass("activated")

            // Ajout de la classe activated à l'élément cliqué pour changer le background et la couleur de texte
            // Cela montre à l'utilisateur dans quelle section il est
            $(this).addClass("activated")

            // Retrait de la classe section_showed à toutes les sections
            $(".section").removeClass("section_showed")
            // Récupération de la section correspondant à l'élément cliqué grâce à l'id
            id = $(this).prop("id")
            var section = $(this).parent().parent().find("." + id)

            // Ajout de la classe section_showed à la section correspondant à l'élément cliqué
            // Celle-ci devient la seule visible pour l'utilisateur
            section.addClass("section_showed")

        })


        //  FONCTIONS GENERALES POUR LA PAGE

        // Fonction permettant de déselectionner toutes les options d'un <select> avec un id choisi en paramètre 
        function unselectOption(selected) {
            $("#" + selected + " option").each(function (index, element) {

                $(element).prop('selected', false)
            });
        }

        // Fonction permettant de sélectionner une option d'un <select> avec un id choisi en paramètre
        // La fonction est utilisée après un requête AJAX pour récupérer un élément ) modifier
        function selectOption(toSelect, condition) {
            $("#" + toSelect + " option").each(function (index, element) {

                if ($(element).val() == condition) {
                    $(element).prop('selected', true)

                }
            });
        }


        // Fonction permettant de faire apparaître la modal
        function showModal(modalName) {
            $('#' + modalName).modal('show');
        }


        // PARTIE ADMIN CONCERNANT LES MEMBRES DE L'EQUIPE

        // Fonction permettant de vérifier le role d'un membre dans la modal
        // En fonction du résultat cela montre ou masque l'input pour le poste des joueuses
        function checkRole() {
            var positionInputBlock = $("#position_input_block")
            if ($("#role").val() == 0) {
                positionInputBlock.css("visibility", "visible")
                positionInputBlock.find("select").prop("disabled", false)

            } else {
                positionInputBlock.css("visibility", "hidden")
                positionInputBlock.find("select").prop("disabled", true)
            }
        }



        // Ouverture de la modal pour ajouter un membre
        $(document).on('click', ".add_staff", function () {
            // Les options qui auraient pu être sélectionnés ne le sont plus
            unselectOption("role")
            unselectOption("position")

            // Les inputs sont vidés dans le cas où le précédent modal était une modification d'un membre
            $("#lastname").val("")
            $("#firstname").val("")
            // Vérification du rôle pour afficher/masquer le poste
            checkRole()
            // Message d'erreur remis à vide à l'ouverture de la modal
            $(".click_message").html("");
            // Titre de la modal et nom du bouton pour valider adaptés à l'ajout d'un membre
            $('#addStaffModal .modal-header h5').html("Ajouter un membre");
            $(".staff_valid_button").html("Ajouter");
            // Mise à 0 de la valeur de l'input hidden pour confirmer qu'il s'agit d'un ajout et pas d'une modification
            // La vérification se fait dans la page member_actions
            $('#member_id').val(0)
            // Apparition de la modal pour les membres de l'équipe
            showModal("addStaffModal")

        });

        // Import de l'id du membre de l'équipe dans la modal et apparition de cette dernière
        $(document).on('click', '.edit_staff', function () {
            // Réinitialisation de la modal et ajout du titre/nom de bouton pour la modification de membre
            unselectOption("role")
            unselectOption("position")
            $(".click_message").html("")
            $('#addStaffModal .modal-header h5').html("Modifier un membre");
            $(".staff_valid_button").html("Modifier");

            // Récupération de l'id du membre à modifier pour l'ajouter en valeur de l'input hidden de la modal
            id = $(this).parent().parent().prop('id')
            $("#member_id").val(id)

            object = "member"
            // Envoi de l'id pour récupérer le membre avec une requête en back
            $.ajax({
                url: 'get_one_object.php',
                type: 'POST',
                data: {
                    id: id,
                    object: object
                },
                dataType: 'JSON',
                success: function (callback) {
                    // Chargement des données correspondant au membre sélectionné dans les input de la modal
                    $("#lastname").val(callback.lastname);
                    $('#firstname').val(callback.firstname)

                    selectOption("role", callback.role)


                    if (callback.role == 0) {

                        selectOption("position", callback.position)
                    }

                    // Vérification du rôle pour afficher/masquer le poste
                    checkRole()

                    // Affichage de la modal
                    showModal("addStaffModal")

                }
            });

        });

        // Vérification du rôle pour afficher/masquer le poste lorsque l'admin change le rôle via le sélecteur
        $("#role_input_block").change(function () {
            checkRole()
        })

        //  Ajout/Modification d'un membre au clic avec vérification dans le back sur la page member_actions
        $(".staff_valid_button").on("click", function (event) {
            // Retire la fonctionnalité initiale du bouton pour qu'il prenne en compte le reste des instructions
            event.preventDefault();

            // Récupération des valeurs des inputs du formulaire
            // Envoi en POST de ces informations avec l'action pour un traitement en back
            id = $('#member_id').val()
            var lastname = $("#lastname").val()
            var firstname = $("#firstname").val()
            var role = $("#role").val()
            var position;
            if (role == "0") {
                position = $("#position").val()
            } else {
                position = null;
            }
            action = 'add'

            // Affichage d'un message d'erreur si un champs est vide
            if (lastname == "" || firstname == "") {
                $(".click_message").html("Il faut remplir tous les champs")
            } else {
                // Sinon l'envoi en POST s'effectue
                $.ajax({
                    url: 'member_actions.php',
                    type: 'POST',
                    data: {
                        id: id,
                        lastname: lastname,
                        firstname: firstname,
                        role: role,
                        position: position,
                        action: action
                    },
                    success: function (callback) {
                        // Affichage d'un message confirmant l'ajout ou la modification
                        if ($('#member_id').val() == 0) {
                            $(".click_message").html("Nouveau membre ajouté")
                        } else {
                            $(".click_message").html("Modifications enregistrées")
                        }
                        // Le tableau contenant tous les membres est chargé à nouveau pour ajouter le nouveau membre ou les modifications
                        $("#staff_table_body").html(callback)
                    }
                });
            }

        })


        // Suppression d'un membre de l'équipe au clic
        $(document).on('click', '.delete_staff', function () {
            // Envoi en POST de l'action demandée et de l'id du membre à supprimer
            // Les vérifications se font au niveau du back sur la page member_actions
            action = 'delete'
            id = $(this).parent().parent().prop('id')
            $.ajax({
                url: 'member_actions.php',
                type: 'POST',
                data: {
                    id: id,
                    action: action
                },
                success: function (callback) {
                    // Le tableau contenant tous les membres est chargé à nouveau pour retirer visuellement le membre supprimé
                    $("#staff_table_body").html(callback)
                }
            });
        })





        // PARTIE ADMIN CONCERNANT LES ENTRAINEMENTS

        // Ouverture de la modal pour ajouter un entraînement
        $(document).on('click', ".add_training", function () {
            // Les options qui auraient pu être sélectionnés ne le sont plus
            unselectOption("day")
            unselectOption("coach")

            // Les inputs sont vidés dans le cas où le précédent modal était une modification d'un entraînement
            $("#begin").val("")
            $("#end").val("")
            $("#room").val("")

            // Message d'erreur remis à vide à l'ouverture de la modal
            $(".click_message").html("");
            // Titre de la modal et nom du bouton pour valider adaptés à l'ajout d'un entraînement
            $('#addTrainingModal .modal-header h5').html("Ajouter un entraînement");
            $(".training_valid_button").html("Ajouter");
            // Mise à 0 de la valeur de l'input hidden pour confirmer qu'il s'agit d'un ajout et pas d'une modification
            // La vérification se fait dans la page trainings_actions
            $('#training_id').val(0)
            // Apparition de la modal pour les entraînements
            showModal("addTrainingModal")

        });

        // Import de l'id de l'entraînement dans la modal et apparition de cette dernière
        $(document).on('click', '.edit_training', function () {
            // Réinitialisation de la modal et ajout du titre/nom de bouton pour la modification de l'entraînement
            unselectOption("day")
            unselectOption("coach")
            $(".click_message").html("")
            $('#addTrainingModal .modal-header h5').html("Modifier un entraînement");
            $(".training_valid_button").html("Modifier");

            // Récupération de l'id de l'entraînement à modifier pour l'ajouter en valeur de l'input hidden de la modal
            id = $(this).parent().parent().prop('id')
            $("#training_id").val(id)

            object = "training"
            // Envoi de l'id pour récupérer l'entraînement avec une requête en back
            $.ajax({
                url: 'get_one_object.php',
                type: 'POST',
                data: {
                    id: id,
                    object: object
                },
                dataType: 'JSON',
                success: function (callback) {
                    // Chargement des données correspondant à l'entraînement sélectionné dans les input de la modal
                    selectOption("day", callback.day)
                    $("#begin").val(callback.begin);
                    $('#end').val(callback.end)
                    $('#room').val(callback.room)
                    $("#coach option").each(function (index, element) {
                        if ($(element).html() == (callback.coach)) {
                            $(element).prop('selected', true)
                        }
                    });

                    // Affichage de la modal
                    showModal("addTrainingModal")

                },
            });

        });

        //  Ajout/Modification d'un entraînement au clic avec vérification dans le back sur la page trainings_actions
        $(".training_valid_button").on("click", function (event) {
            // Retire la fonctionnalité initiale du bouton pour qu'il prenne en compte le reste des instructions
            event.preventDefault();

            // Récupération des valeurs des inputs du formulaire
            // Envoi en POST de ces informations avec l'action pour un traitement en back
            id = $('#training_id').val()
            var day = $("#day").val()
            var begin = $("#begin").val()
            var end = $("#end").val()
            var room = $("#room").val();
            var coach = $("#coach").val();
            action = 'add';

            // Affichage d'un message d'erreur si un champs est vide
            if (room == "" || begin == "" || end == "") {
                $(".click_message").html("Il faut remplir tous les champs")
            } else {
                // Sinon l'envoi en POST s'effectue
                $.ajax({
                    url: 'trainings_actions.php',
                    type: 'POST',
                    data: {
                        id: id,
                        day: day,
                        begin: begin,
                        end: end,
                        room: room,
                        coach: coach,
                        action: action
                    },
                    success: function (callback) {
                        // Affichage d'un message confirmant l'ajout ou la modification
                        if ($('#training_id').val() == 0) {
                            $(".click_message").html("Nouvel entraînement ajouté")
                        } else {
                            $(".click_message").html("Modifications enregistrées")
                        }
                        // Le tableau contenant tous les entraînements est chargé à nouveau pour ajouter le nouvel entraînement ou les modifications
                        $("#trainings_table_body").html(callback)
                    }
                });
            }

        })


        // Suppression d'un entraînement au clic
        $(document).on('click', '.delete_training', function () {
            // Envoi en POST de l'action demandée et de l'id de l'entraînement à supprimer
            // Les vérifications se font au niveau du back sur la page trainings_actions
            action = 'delete'
            id = $(this).parent().parent().prop('id')
            $.ajax({
                url: 'trainings_actions.php',
                type: 'POST',
                data: {
                    id: id,
                    action: action
                },
                success: function (callback) {
                    // Le tableau contenant tous les entraînements est chargé à nouveau pour retirer visuellement l'entraînement supprimé
                    $("#trainings_table_body").html(callback)
                }
            });
        })



        // PARTIE ADMIN CONCERNANT LES MATCHS


        // Ouverture de la modal pour ajouter un match
        $(document).on('click', ".add_game", function () {
            // Les inputs sont vidés dans le cas où le précédent modal était une modification d'un match
            $("#date").val("")
            $("#hour").val("")
            $("#opponent").val("")


            // Message d'erreur remis à vide à l'ouverture de la modal
            $(".click_message").html("");
            // Titre de la modal et nom du bouton pour valider adaptés à l'ajout d'un entraînement
            $('#addGameModal .modal-header h5').html("Ajouter un match");
            $(".game_valid_button").html("Ajouter");
            // Mise à 0 de la valeur de l'input hidden pour confirmer qu'il s'agit d'un ajout et pas d'une modification
            // La vérification se fait dans la page games_actions
            $('#game_id').val(0)
            // Apparition de la modal pour les matchs
            showModal("addGameModal")

        });

        // Import de l'id du match dans la modal et apparition de cette dernière
        $(document).on('click', '.edit_game', function () {
            // Réinitialisation de la modal et ajout du titre/nom de bouton pour la modification du match
            $(".click_message").html("")
            $('#addGameModal .modal-header h5').html("Modifier un match");
            $(".game_valid_button").html("Modifier");

            // Récupération de l'id du match à modifier pour l'ajouter en valeur de l'input hidden de la modal
            id = $(this).parent().parent().prop('id')
            $("#game_id").val(id)

            object = "game"

            // Envoi de l'id pour récupérer le match avec une requête en back

            $.ajax({
                url: 'get_one_object.php',
                type: 'POST',
                data: {
                    id: id,
                    object: object
                },
                dataType: 'JSON',
                success: function (callback) {
                    // Chargement des données correspondant au match sélectionné dans les input de la modal
                    $("#date").val(callback.date);
                    $('#hour').val(callback.hour)
                    $('#opponent').val(callback.opponent)

                    // Affichage de la modal
                    showModal("addGameModal")


                },
            });

        });

        //  Ajout/Modification d'un match au clic avec vérification dans le back sur la page games_actions
        $(".game_valid_button").on("click", function (event) {
            // Retire la fonctionnalité initiale du bouton pour qu'il prenne en compte le reste des instructions
            event.preventDefault();

            // Récupération des valeurs des inputs du formulaire
            // Envoi en POST de ces informations avec l'action pour un traitement en back
            id = $('#game_id').val()
            var date = $("#date").val()
            var hour = $("#hour").val()
            var opponent = $("#opponent").val()
            action = 'add';

            // Paramètres pour vérifier que la date envoyée est supérieure ou égale à la date du jour
            var actualDate = new Date()
            var postedDate = new Date(date)
            actualDate.setHours(0, 0, 0, 0)

            // Affichage d'un message d'erreur si un champs est vide
            if (date == "" || hour == "" || opponent == "") {
                $(".click_message").html("Il faut remplir tous les champs")

                //  Affichage d'un message d'erreur si la date du jour est supérieure à la date envoyée
            } else if (actualDate > postedDate) {
                $(".click_message").html("La date sélectionnée ne peut être antérieure à celle d'ajourd'hui")

            } else {
                // Sinon l'envoi en POST s'effectue
                $.ajax({
                    url: 'games_actions.php',
                    type: 'POST',
                    data: {
                        id: id,
                        date: date,
                        hour: hour,
                        opponent: opponent,
                        action: action
                    },
                    success: function (callback) {
                        // Affichage d'un message confirmant l'ajout ou la modification
                        if ($('#game_id').val() == 0) {
                            $(".click_message").html("Nouveau match ajouté")
                        } else {
                            $(".click_message").html("Modifications enregistrées")
                        }
                        // Le tableau contenant tous les entraînements est chargé à nouveau pour ajouter le nouvel entraînement ou les modifications
                        $("#games_table_body").html(callback)
                    }
                });
            }

        })


        // Suppression d'un match au clic
        $(document).on('click', '.delete_game', function () {
            // Envoi en POST de l'action demandée et de l'id du match à supprimer
            // Les vérifications se font au niveau du back sur la page games_actions
            action = 'delete'
            id = $(this).parent().parent().prop('id')
            $.ajax({
                url: 'games_actions.php',
                type: 'POST',
                data: {
                    id: id,
                    action: action
                },
                success: function (callback) {
                    // Le tableau contenant tous les matchs est chargé à nouveau pour retirer visuellement l'entraînement supprimé
                    $("#games_table_body").html(callback)
                }
            });
        })




        // PARTIE ADMIN CONCERNANT LES ADMINISTRATEURS

        // Fonction permettant d'afficher/masquer les champs de saisie pour le mot de passe
        function togglePassword(toggle) {
            var passInputBlock = $(".pass_input_block")
            if (toggle) {
                passInputBlock.each(function (index, element) {
                    $(element).css("display", "flex")
                    $(element).find("select").prop("disabled", false)
                })
            } else {

                passInputBlock.each(function (index, element) {
                    $(element).css("display", "none")
                    $(element).find("select").prop("disabled", true)
                })
            }
        }



        // Ouverture de la modal pour ajouter un admin
        $(document).on('click', ".add_admin", function () {
            // Les options qui auraient pu être sélectionnés ne le sont plus
            unselectOption("admin_role")

            // activePass devient true pour que l'ajout d'un admin soit détecté plutôt que la modification
            activePass = true;
            togglePassword(activePass)

            // Les inputs sont vidés dans le cas où le précédent modal était une modification d'un admin
            $("#username").val("")
            $("#password").val("")
            $("#confirm_password").val("")


            // Message d'erreur remis à vide à l'ouverture de la modal
            $(".click_message").html("");
            // Titre de la modal et nom du bouton pour valider adaptés à l'ajout d'un admin
            $('#addAdminModal .modal-header h5').html("Ajouter un admin");
            $(".admin_valid_button").html("Ajouter");
            // Mise à 0 de la valeur de l'input hidden pour confirmer qu'il s'agit d'un ajout et pas d'une modification
            // La vérification se fait dans la page admins_actions
            $('#admin_id').val(0)
            // Apparition de la modal pour les admins
            showModal("addAdminModal")

        });

        // Import de l'id de l'admin dans la modal et apparition de cette dernière
        $(document).on('click', '.edit_admin', function () {
            // Réinitialisation de la modal et ajout du titre/nom de bouton pour la modification d'admin
            unselectOption("admin_role")

            $(".click_message").html("")
            $('#addAdminModal .modal-header h5').html("Modifier un admin");
            $(".admin_valid_button").html("Modifier");

            // activePass devient false pour que la modification d'un admin soit détecté plutôt que l'ajout
            activePass = false;
            togglePassword(activePass)

            // Récupération de l'id de l'admin à modifier pour l'ajouter en valeur de l'input hidden de la modal
            id = $(this).parent().parent().prop('id')
            $("#admin_id").val(id)

            object = "admin"
            // Envoi de l'id pour récupérer l'admin avec une requête en back
            $.ajax({
                url: 'get_one_object.php',
                type: 'POST',
                data: {
                    id: id,
                    object: object
                },
                dataType: 'JSON',
                success: function (callback) {
                    // Chargement des données correspondant à l'admin sélectionné dans les input de la modal
                    $("#username").val(callback.username);

                    selectOption("admin_role", callback.role)

                    // Affichage de la modal
                    showModal("addAdminModal")

                }
            });

        });


        //  Ajout/Modification d'un admin au clic avec vérification dans le back sur la page admins_actions
        $(".admin_valid_button").on("click", function (event) {
            // Retire la fonctionnalité initiale du bouton pour qu'il prenne en compte le reste des instructions
            event.preventDefault();

            // Récupération des valeurs des inputs du formulaire
            // Envoi en POST de ces informations avec l'action pour un traitement en back
            id = $('#admin_id').val()
            var username = $("#username").val()
            var adminRole = $("#admin_role").val()
            var password;
            // Si c'est un ajout, on vérifie que le mot de passe n'est pas vide et qu'il est confirmé dans le 2ème champs de saisie
            if (activePass) {
                if ($("password").val() == "") {
                    password = ""
                } else if ($("password").val() != "" && $("#password").val() != $("#confirm_password").val()) {
                    password = "error"
                } else {
                    password = $("#password").val()
                }
            } else {
                password = null
            }

            action = 'add'

            // Affichage d'un message d'erreur si un champs est vide
            if (username == "" || password == "") {
                $(".click_message").html("Il faut remplir tous les champs")
                // Affichage d'un message d'erreur le champs de confirmation n'est pas le même que le mot de passe saisi
            } else if (username != "" && password == "error") {
                $(".click_message").html("Il faut confirmer le mot de passe")
            } else {
                // Sinon l'envoi en POST s'effectue
                $.ajax({
                    url: 'admins_actions.php',
                    type: 'POST',
                    data: {
                        id: id,
                        username: username,
                        adminRole: adminRole,
                        password: password,
                        action: action
                    },
                    success: function (callback) {
                        // Test si le callback reçu est bien sous format JSON et affiche un message d'erreur à l'utilisateur
                        try {
                            JSON.parse(callback);
                            $(".click_message").html("Nom d'utilisateur déjà existant")

                            // Si ce n'est pas le cas, le nouvel admin est ajouté au tableau
                        } catch (e) {

                            // Affichage d'un message confirmant l'ajout ou la modification
                            if ($('#admin_id').val() == 0) {
                                $(".click_message").html("Nouvel admin ajouté")
                            } else {
                                $(".click_message").html("Modifications enregistrées")
                            }
                            // Le tableau contenant tous les admins est chargé à nouveau pour ajouter le nouvel admin ou les modifications
                            $("#admin_table_body").html(callback)
                        }
                    }
                });
            }

        })


        // Suppression d'un admin au clic
        $(document).on('click', '.delete_admin', function () {
            // Envoi en POST de l'action demandée et de l'id de l'admin à supprimer
            // Les vérifications se font au niveau du back sur la page admins_actions
            action = 'delete'
            id = $(this).parent().parent().prop('id')
            $.ajax({
                url: 'admins_actions.php',
                type: 'POST',
                data: {
                    id: id,
                    action: action
                },
                success: function (callback) {
                    // Le tableau contenant tous les admins est chargé à nouveau pour retirer visuellement l'admin supprimé
                    $("#admin_table_body").html(callback)
                }
            });
        })




        // PARTIE ADMIN CONCERNANT LES ARTICLES

        // Ouverture de la modal pour ajouter un article
        $(document).on('click', ".add_article", function () {
            // Les inputs sont vidés dans le cas où le précédent modal était une modification d'un article
            $('#image').val('')
            $("#title").val("")
            $("#publish").prop("checked", false)
            tinymce.get('content').setContent("");
            $("#publish").prop("checked", false)
            // Message d'erreur remis à vide à l'ouverture de la modal
            $(".click_message").html("");
            // Titre de la modal et nom du bouton pour valider adaptés à l'ajout d'un article
            $('#addArticleModal .modal-header h5').html("Ajouter un article");
            $(".article_valid_button").html("Ajouter");
            // Mise à 0 de la valeur de l'input hidden pour confirmer qu'il s'agit d'un ajout et pas d'une modification
            // La vérification se fait dans la page articles_actions
            $('#article_id').val(0)

            // Variable permettant par la suite de vérifier qu'il s'agit d'un ajout et donc qu'une image doit être ajoutée
            sendImage = true;

            // Apparition de la modal pour les articles
            showModal("addArticleModal")

        });

        // Import de l'id de l'article dans la modal et apparition de cette dernière
        $(document).on('click', '.edit_article', function () {
            // Réinitialisation de la modal et ajout du titre/nom de bouton pour la modification d'article
            $('#image').val('')
            $(".click_message").html("")
            $('#addArticleModal .modal-header h5').html("Modifier un article");
            $(".article_valid_button").html("Modifier");

            // Récupération de l'id de l'article à modifier pour l'ajouter en valeur de l'input hidden de la modal
            id = $(this).parent().parent().prop('id')
            $("#article_id").val(id)

            object = "article"

            // Variable permettant par la suite de vérifier qu'il s'agit d'une modification
            // Donc ajouter une image n'est pas obligatoire car il y en a déjà une en BDD
            sendImage = false;

            // Envoi de l'id pour récupérer l'article avec une requête en back
            $.ajax({
                url: 'get_one_object.php',
                type: 'POST',
                data: {
                    id: id,
                    object: object
                },
                dataType: 'JSON',
                success: function (callback) {
                    // Chargement des données correspondant à l'article sélectionné dans les input de la modal
                    $("#title").val(callback.title);
                    tinymce.get('content').setContent(callback.content);
                    if (callback.publish == 1) {
                        $("#publish").prop("checked", true)
                    } else {
                        $("#publish").prop("checked", false)
                    }

                    // Affichage de la modal
                    showModal("addArticleModal")

                }
            });

        });


        //  Ajout/Modification d'un article au clic avec vérification dans le back sur la page articles_actions
        $(".article_valid_button").on("click", function (event) {
            // Retire la fonctionnalité initiale du bouton pour qu'il prenne en compte le reste des instructions
            event.preventDefault();

            // Récupération des valeurs des inputs du formulaire
            // Envoi en POST de ces informations avec l'action pour un traitement en back
            id = $('#article_id').val()
            var title = $("#title").val()
            var content = tinymce.get('content').getContent();
            var article_date = $("#article_date").val()
            var publish
            if ($("#publish").prop("checked") == true) {
                publish = 1
            } else {
                publish = 0
            }
            var data = sessionStorage.getItem('logged');
            console.log($("#publish"))
            action = 'add';
            var image = $('#image')[0].files[0]

            // Création d'un objet FormData pour enregistrer les valeurs des input dont l'image ajoutée par l'admin
            var formData = new FormData();
            formData.append('image', image);
            formData.append('id', id);
            formData.append('title', title);
            formData.append('content', content);
            formData.append('article_date', article_date);
            formData.append('publish', publish);
            formData.append('action', action);

            // S'il s'agit d'un ajout et qu'un champs est vide, un message d'erreur apparaît
            if (sendImage == true && (title == "" || content == "" || image == undefined)) {
                $(".click_message").html("Il faut remplir tous les champs")

            // S'il s'agit d'une modification et qu'un champs est vide hormis l'image, un message d'erreur apparaît
            } else if(sendImage = false && (title == "" || content == ""))  {
                $(".click_message").html("Il faut remplir tous les champs")
            } else {
                // Sinon l'envoi en POST s'effectue
                $.ajax({
                    url: 'articles_actions.php',
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (callback) {
                        // Test si le callback reçu est bien sous format JSON et affiche un message d'erreur à l'utilisateur
                        try {
                            var json_callback = JSON.parse(callback);

                            // Affiche un message si l'image envoyée est trop volumineuse
                            if (json_callback.message == "size") {
                                $(".click_message").html("Fichier trop volumineux")
                            // Affiche un message si le format de l'image n'est pas bon
                            } else {
                                $(".click_message").html("Envoyez un fichier uniquement avec les extensions .png, .jpg ou .jpeg")
                            }

                            // Si ce n'est pas le cas, le nouvel article est ajouté au tableau
                        } catch (e) {
                            // Affichage d'un message confirmant l'ajout ou la modification
                            if ($('#article_id').val() == 0) {
                                $(".click_message").html("Nouvel article ajouté")
                            } else {
                                $(".click_message").html("Modifications enregistrées")
                            }
                            // Le tableau contenant tous les articles est chargé à nouveau pour ajouter le nouvel article ou les modifications
                            $("#articles_table_body").html(callback)
                        }

                    }
                });
            }

        })


        // Suppression d'un article au clic
        $(document).on('click', '.delete_article', function () {
            // Envoi en POST de l'action demandée et de l'id de l'article à supprimer
            // Les vérifications se font au niveau du back sur la page articles_actions
            action = 'delete'
            id = $(this).parent().parent().prop('id')
            $.ajax({
                url: 'articles_actions.php',
                type: 'POST',
                data: {
                    id: id,
                    action: action
                },
                success: function (callback) {
                    // Le tableau contenant tous les articles est chargé à nouveau pour retirer visuellement l'article supprimé
                    $("#articles_table_body").html(callback)
                }
            });
        })
    }
})