// Attends que la page soit chargée avant d'éxécuter le script
$(document).ready(function () { // CONTENU DU MENU AJOUTE AU MENU RESPONSIVE

    var contentMenu = $("#menu").html()
    var responsiveMenu = $("#list_menu")
    responsiveMenu.html(contentMenu)

    var menuIcon = $(".fa-bars")
    var closeIcon = $(".fa-times")

    // APPARITION DU MENU RESPONSIVE AU CLIC SUR LE BOUTON HAMBURGER
    menuIcon.click(function () {
        $(this).css("display", "none")
        closeIcon.css("display", "block")
        responsiveMenu.css("display", "block")
    })

    // DISPARITION DU MENU RESPONSIVE AU CLIC SUR LE BOUTON CROIX
    closeIcon.click(function () {
        $(this).css("display", "none")
        menuIcon.css("display", "block")
        responsiveMenu.css("display", "none")

    })

    // Retrait de la classe current_page à tous les liens du menu de navigation
    $(".nav_link").removeClass("current_page")

    // Ajout de la classe current_page au lien du menu correspondant à la page en cours 
    $(".nav_link").each((index, element) => {
        
       if($(element).attr("id") == page) {
           $(element).addClass("current_page")
       }
    })
})