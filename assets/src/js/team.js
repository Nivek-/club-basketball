// Attends que la page soit chargée avant d'éxécuter le script
$(document).ready(function () {

    // Vérifie que la page est bien 'les équipes féminines' et que la largeur de l'écran est supérieure à 800px
    if (page == "team" && window.innerWidth > 800) {
        
        // Tableau contenant les 5 postes des joueuses
        var positions = ["point_guard", "shooting_guard", "small_forward", "power_forward", "center"]


        // Permet afficher la poste d'une joueuse sur le terrain lorsque la souris passe sur sa ligne dans le tableau
        $(".field_position").mouseenter(function () {

            // Itération sur chaque élément du tableau des postes
            positions.forEach(position => {
                // Si un élément du tableau correspond au poste de la joueuse (indiqué dans une classe)
                // Le bloc contenant le nom et la position du poste sur le terrain est affiché 
                if ($(this).hasClass(position)) {
                    $("#" + position).removeClass("hidden")
                }
            });
        })


        // Permet de masquer le poste d'une joueuse qui vient d'être affiché lorsque la souris sort de sa ligne dans le tableau
        $(".field_position").mouseleave(function () {

            // Itération sur chaque élément du tableau des postes
            positions.forEach(position => {
                // Si un élément du tableau correspond au poste de la joueuse (indiqué dans une classe)
                // Le bloc précédemment affiché est masqué à nouveau
                if ($(this).hasClass(position)) {
                    $("#" + position).addClass("hidden")
                }
            });
        })
    }

})