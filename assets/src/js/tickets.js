$(document).ready(function () {
    // Vérifie que la page actuelle est la page tickets

    if (page == "tickets") {

        // Initialisation de variables utilisées dans les fonctions suivantes
        var prices = ["#950", "#1150", "#1300"];
        var amount;
        var availableDiscount;


        // FONCTION CALCULANT LE MONTANT TOTAL 

        function calcAmount() {
            //Remise à 0 des variables dont la valeur va être recalculée dans la boucle
            availableDiscount = 0;
            amount = 0;

            // Boucle sur le tableau "prices" contenant les différents prix des places
            for (var i = 0; i < prices.length; i++) {
                // Récupération de la valeur de l'input pour chaque catégorie de prix
                var input = $(prices[i]);
                var nbr = input.val();

                // Ajout de la valeur de l'input dans une variable pour connaître le nombre de réductions disponibles
                availableDiscount += parseInt(nbr);

                // Récupération du prix enregistré dans l'id de l'input et mis sous la forme "x,xx"
                var price = parseFloat(input.attr("id")) / 100;

                // Produit du nombre de places multiplié par le prix de la place ajouté dans le montant total pour chaque catégorie de prix
                amount += nbr * price;
            }

            // Appel de la fonction concernant la réduction de 2€
            discountChange()

            // Produit du nombre de réductions multiplié par 2 (prix de la réduction) soustrait au montant total
            var discount = $("#200");
            amount -= 2 * discount.val();
        }



        // FONCTION AUTORISANT/INTERDISANT L'UTILISATEUR D'AUGMENTER/DIMINUER LE NOMBRE DE REDUCTIONS

        function discountChange() {
            $discountQuantity = $(".discount_number");
            // S'il y a plus de réductions que de places ajoutées le nombre de réductions est réduit pour égaler le nombre de places
            if ($discountQuantity.val() > availableDiscount) {
                $discountQuantity.val(availableDiscount)
                $(".discount_plus").addClass("inactive");
            }

            // Si le nombre de réduction est à 0
            if ($discountQuantity.val() == 0) {
                // Le retrait de réductions est désactivé
                $(".discount_minus").addClass("inactive");
                
                // Et que le nombre de réductions disponibles est à 0
                if (availableDiscount == 0) {
                    // L'ajout de réductions est désactivé
                    $(".discount_plus").addClass("inactive");

                    // Et si le nombre de réductions disponibles est supérieur à 0
                } else {
                    // L'ajout est activé
                    $(".discount_plus").removeClass("inactive");
                }

                // Si le nombre de réduction est supérieur à 0 
            } else {
                // Le retrait est activé
                $(".discount_minus").removeClass("inactive");

                // Et que le nombre de réductions est inférieur au nombre de réductions disponibles
                if ($discountQuantity.val() < availableDiscount) {
                    // L'ajout est activé
                    $(".discount_plus").removeClass("inactive");

                // Et si le nombre de réductions est égal au nombre de réductions
                } else {
                    // L'ajout es désactivé
                    $(".discount_plus").addClass("inactive");
                }
            }
        }


        // Appel de la fonction de calcul au lancement de la page
        calcAmount()
        amountInt = parseFloat(amount).toFixed(2);
        // Ajout du montant en face de total
        $("#amount").html(amountInt);


        // ACTIONS SE DECLANCHANT AU CLIC D'UN BOUTON PLUS OU MOINS

        $(".change_value").click(function (event) {

            // Enregistrement dans une variable de l'input présent dans la même balise parent que l'élément cliqué
            var inputValue = $(this).parent().children("input");
            // Transformation en nombre et enregistrement dans une variable de la valeur de l'input présent dans la même balise parent que l'élément cliqué
            var number = parseInt($(this).parent().children("input").val());

            // Si l'élément cliqué est un plus
            if ($(this).hasClass("fa-plus")) {
                // Un est ajouté à la valeur de l'input
                inputValue.val(number + 1);
                // Le retrait est activé pour cet input
                $(this).parent().children(".fa-minus").removeClass("inactive");

                // Nouveau calcul du montant
                calcAmount();

                // Si l'élément cliqué est un moins
            } else {
                // Un est retiré à la valeur de l'input
                inputValue.val(number - 1);
                // Si la valeur de l'input tombe à 0 le retrait est désactivé
                if (number - 1 == 0) {
                    $(this).addClass("inactive")
                }

                // Nouveau calcul du montant
                calcAmount();

            }


            amountInt = parseFloat(amount).toFixed(2);
            // Ajout du montant en face de total
            $("#amount").html(amountInt);
        })
    }
})