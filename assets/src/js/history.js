// Attends que la page soit chargée avant d'éxécuter le script
$(document).ready(function () {

	// Vérifie que la page est bien 'history'
	if (page == "history") {

		// Mise en place de la librairie ScrollReveal importée dans l'index
		window.sr = ScrollReveal();

		// Si la taille de l'écran est inférieur à 767px
		if ($(window).width() < 767) {

			// Les cartes placées à gauche arrivent par la droite
			if ($('.card_content').hasClass('from_left')) {
				$('.card_content').removeClass('from_left').addClass('from_right');
			}

			// Les cartes arrivent par la droite au scroll
			sr.reveal('.from_right', {
				origin: 'right',
				distance: '300px',
				easing: 'ease',
				duration: 1000,
			});

		} else {

			// Les cartes placées à gauche de la timeline arrivent par la gauche au scroll
			sr.reveal('.from_left', {
				origin: 'left',
				distance: '300px',
				easing: 'ease',
				duration: 1000,
			});

			// Les cartes placées à droite de la timeline arrivent par la droite au scroll
			sr.reveal('.from_right', {
				origin: 'right',
				distance: '300px',
				easing: 'ease',
				duration: 1000,
			});

		}
	}
})