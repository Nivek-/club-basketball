<?php
require_once "autoload.php";

if (isset($_POST["object"])) {
    // Vérifications pour savoir quel élément est à modifier

    if ($_POST["object"] == "member") {
        // Création d'un nouvel objet de la classe Staff qui récupère les informations du membre correspondant à l'ID envoyé via l'AJAX
        $staff = new Staff();
        $staff->getStaffMember($_POST["id"]);

        // Renvoi des informations concernant le membre de l'équipe pour les réutiliser sur la partie front
        echo json_encode(array('lastname' => $staff->getLastname(), 'firstname' => $staff->getFirstname(), 'role' => $staff->getRole(), 'position' => $staff->getPosition()));

    } else if ($_POST["object"] == "training") {
        // Création d'un nouvel objet de la classe Training qui récupère les informations de l'entraînement correspondant à l'ID envoyé via l'AJAX
        $training = new Training();
        $training->getTraining($_POST["id"]);

        // Renvoi des informations concernant l'entraînement pour les réutiliser sur la partie front
        echo json_encode(array('day' => $training->getDay(), 'begin' => $training->getBegin(), 'end' => $training->getEnd(), 'room' => $training->getRoom(), 'coach' => $training->getCoachId()));

    } else if ($_POST["object"] == "game") {
        // Création d'un nouvel objet de la classe Game qui récupère les informations du match correspondant à l'ID envoyé via l'AJAX
        $game = new Game();
        $game->getGame($_POST["id"]);
        $game_date = date('Y-m-d', $game->getDateGame());

        // Renvoi des informations concernant le match pour les réutiliser sur la partie front
        echo json_encode(array('date' => $game_date, 'hour' => $game->getHour(), 'opponent' => $game->getOpponent()));

    } else if ($_POST["object"] == "admin") {
        // Création d'un nouvel objet de la classe Admin qui récupère les informations de l'admin correspondant à l'ID envoyé via l'AJAX
        $admin = new Admin();
        $admin->getAdmin($_POST["id"]);

        // Renvoi des informations concernant l'admin pour les réutiliser sur la partie front
        echo json_encode(array('username' => $admin->getUsername(), 'role' => $admin->getRole()));

    } else if ($_POST["object"] == "article") {
        // Création d'un nouvel objet de la classe Article qui récupère les informations de l'article correspondant à l'ID envoyé via l'AJAX
        $article = new Article();
        $article->getArticle($_POST["id"]);

        // Renvoi des informations concernant l'article pour les réutiliser sur la partie front
        echo json_encode(array('title' => $article->getTitle(), 'content' => $article->getContent(), 'date' => $article->getArticleDate(), 'publish' => $article->getPublish(), 'image' => $article->getImage()));
    }
}
