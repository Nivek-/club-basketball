<div class='container'>

    <!-- TITLE OF THE PAGE -->
    <h3>U18 F</h3>

    <!-- TEAM SECTION -->
    <div class='team_informations'>

        <!-- BASKET FIELD AND TRAININGS TABLE -->
        <div class="field_and_trainings">

            <!-- BASKET FIELD WITH BALL/POSITION NAME -->
            <div class='animated_field'>
                <img src="assets/images/full_basketball_field.jpg" alt='terrain de basketball' class='field'>
                <div class='basketball hidden' id='point_guard'>
                    <img src="assets/images/basketball.svg" alt='ballon de basketball'>
                    <p>Meneur</p>
                </div>
                <div class='basketball hidden' id='shooting_guard'>
                    <img src="assets/images/basketball.svg" alt='ballon de basketball'>
                    <p>Arrière</p>
                </div>
                <div class='basketball hidden' id='power_forward'>
                    <img src="assets/images/basketball.svg" alt='ballon de basketball'>
                    <p>Ailier fort</p>
                </div>
                <div  class='basketball hidden' id='center'>
                    <img src="assets/images/basketball.svg" alt='ballon de basketball'>
                    <p>Pivot</p>
                </div>
                <div class='basketball hidden' id='small_forward'>
                    <img src="assets/images/basketball.svg" alt='ballon de basketball'>
                    <p>Ailier</p>
                </div>
            </div>

            <!-- TRAININGS TABLE -->
            <p>Entraînements :</p>
            <table class='trainings_board'>
                <thead>
                        <td>Jour</td>
                        <td>Horaire</td>
                        <td>Salle</td>
                        <td>Coach</td>
                </thead>
                <tbody>
                    <?php

                        // IMPORT AND SHOW TRAININGS FROM DATABASE
                        $trainings = new Training();
                        $training_results = $trainings->getAll();

                        foreach($training_results as $result) {
                                echo "<tr><td>".ucfirst($result->getDay())."</td>";
                                echo "<td>".$result->getBegin()." - ".$result->getEnd()."</td>";
                                echo "<td>".strtoupper($result->getRoom())."</td>";
                                echo "<td>".$result->getCoachId()."</td></tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>

        <!-- STAFF  -->
        <div class="team_staff">

        <!-- PLAYERS TABLE -->
            <table class='team_players_board'>
                <thead>
                    <tr>
                        <td>Nom</td>
                        <td>Prénom</td>
                        <td class='player_position'>Poste</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // IMPORT STAFF FROM DATABASE
                    $staff = new Staff();
                    $staff_results = $staff->getAll();
                    
                    // SHOW PLAYERS
                    foreach($staff_results as $result) {
                        if($result->getRole() == 0) {
                            echo "<tr class='field_position ".$result->getPosition()."'><td>".$result->getLastname()."</td>";
                            echo "<td>".$result->getFirstname()."</td>";
                            $result->translatePosition();
                            echo "<td class='player_position'>".ucfirst($result->translatedPosition)."</td></tr>";
                        }
                    }
                    ?>
                </tbody>
            </table>

            <!-- COACHES -->
            <div class='coaches'>
                <p class='coach_title'>Coach :</p>
                <?php

                // SHOW COACHES
                foreach($staff_results as $result) {
                    if($result->getRole() == 1) {
                        echo "<p>".$result->getLastname()." ".$result->getFirstname()."</td>";
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>

<!-- CONTACT SECTION -->
<div class='covered_section covered_team'>

    <!-- LINK TO CONTACT PAGE -->
    <p>Vous souhaitez rejoindre notre équipe ?
        <a href="index.php?page=contact">Contactez-nous</a> via notre formulaire sans plus attendre !</p>

        <!-- TEAM PICTURE -->
    <img src="assets/images/u18_feminine_basketball_team_avesnes.jpg" alt="équipe féminine u18 du club de basketball d'avesnes">
</div>