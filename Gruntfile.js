module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
      sass: {
        dist: {
          options: {
            style: 'compressed', // Can be nested, compact, compressed, expanded.
            compass: false
          },
          files: { 
            'assets/css/style.min.css': 'assets/src/css/style.scss',
            'assets/css/common.min.css': 'assets/src/css/common.scss',
            'assets/css/header.min.css': 'assets/src/css/header.scss',
            'assets/css/footer.min.css': 'assets/src/css/footer.scss',
            'assets/css/home.min.css': 'assets/src/css/home.scss',
            'assets/css/tickets.min.css': 'assets/src/css/tickets.scss',
            'assets/css/team.min.css': 'assets/src/css/team.scss',
            'assets/css/admin.min.css': 'assets/src/css/admin.scss',
            'assets/css/login.min.css': 'assets/src/css/login.scss',
            'assets/css/modal.min.css': 'assets/src/css/modal.scss',
            'assets/css/contact.min.css': 'assets/src/css/contact.scss',
            'assets/css/article.min.css': 'assets/src/css/article.scss',
            'assets/css/history.min.css': 'assets/src/css/history.scss',
            'assets/css/sitemap.min.css': 'assets/src/css/sitemap.scss',
            'assets/css/legal.min.css': 'assets/src/css/legal.scss',
          }
        }
        },
      uglify: {
        options: {
          banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
        },
        build: {
          src: ['assets/src/js/*.js'],
          dest: 'assets/js/script.min.js'
        }
      },
      imagemin: {
        dynamic: {
            files: [{
                expand: true,
                cwd: 'assets/src/images/',
                src: ['**/*.{png,jpg,gif}'],
                dest: 'assets/images/'
            }]
        }
      },
      watch: {
        styles: {
          files: ['assets/src/css/*.scss'],
          tasks: ['sass']
        },
        scripts: {
          files: ['assets/src/js/*.js'],
          tasks: ['uglify']
        },
      },
      clean: ['assets/js', 'assets/css']
    });
  
    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    grunt.loadNpmTasks("grunt-contrib-sass");
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch')
  
    // Default task(s).
    grunt.registerTask('default', ['clean', 'sass', 'uglify', 'imagemin']);
  
  };