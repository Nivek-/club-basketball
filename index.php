<?php
require_once "autoload.php";
?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name='description' content="Site officiel du club de basketball féminin d'Asvenes dans le département du Nord évoluant au sein de la ligue féminine française de basketball">
  <meta name='keywords' content="club, basketball, officiel, avesnes, féminin, match, basket, nord, ligue, française, Hyper U, Crédit Mutuel, Hauts-de-France, équipe, panier, championnat, terrain">
  <title>Club féminin de basketball d'Avesnes - Site officiel</title>
  <link rel="shortcut icon" href="assets/images/favicon.ico">
  <link rel="stylesheet" href="assets/css/style.min.css">

  <!-- SETUP TINYMCE EDITOR FOR EDIT ARTICLES -->
  <script src="https://cdn.tiny.cloud/1/kk4xh5o86cnsed8rdhjf7vd8kyv246i8s09yn6o3yhvwcss9/tinymce/5/tinymce.min.js" referrerpolicy="origin"/></script>

  <script>
      tinymce.init({
        selector: '#content',
        min_height: 150,
      });
  </script>
  
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>

  <header>
    <?php
// Appel du header
require_once 'header.php';
?>
  </header>

  <section>
    <?php
// Vérification que la page demandée existe bien sinon redirection vers la page d'accueil
if (isset($_GET['page']) && file_exists($_GET['page'] . ".php")) {
    if ($_GET['page'] == "article" && !isset($_GET["id"])) {
        require_once 'home.php';
    } else {
        require_once $_GET['page'] . '.php';
    }
} else {
    require_once 'home.php';
}
?>
  </section>

  <footer>
    <?php
// Appel du footer
require_once 'footer.php';
?>
  </footer>


  <script src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src='https://cdn.jsdelivr.net/scrollreveal.js/3.3.1/scrollreveal.min.js'></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
  </script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
  </script>
  <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

  <script src="assets/js/script.min.js"></script>
</body>

</html>