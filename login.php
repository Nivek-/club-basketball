<?php
    if(isset($_SESSION["logged"])) {
        header("Location: index.php?page=admin");
        exit();
    }
?>

<div class='container'>
    <!-- TITLE -->
    <h3>Connexion</h3>

    
    <?php
        
        // Connexion de l'utilisateur après vérification des champs saisis
        if (isset($_POST["submit_btn"])) {
            if ($_POST["username"] == null || $_POST["password"] == null) {
                echo "<p>Veuillez remplir les deux champs pour vous connecter</p>";
            } else {
                $admin = new Admin();
                $admin->setUsername($_POST["username"]);
                $admin->setPassword($_POST["password"]);
                $logged = $admin->login();
                if(!$logged) {
                    echo "Le nom d'utilisateur et le mot de passe ne correspondent pas";
                }
            }
        }
?>

    <!-- LOGIN FORM -->
    <form class='login' method="POST" action="index.php?page=login">
        <div class="form_input">
            <label for="username">Nom d'utilisateur</label>
            <input type="text" name="username" id="username" />
        </div>
        <div class="form_input">
            <label for="password">Mot de passe</label>
            <input type="password" name="password" id="password" />
        </div>

        <!-- SUBMIT BUTTON -->
        <input type="submit" value="Connexion" name="submit_btn" class="valid_btn page_button">
    </form>

</div>