<div class='container'>

    <!-- ARTICLE CONTENT -->
    <div class='article_page_content'>
        <?php
            // Récupération de l'article grâce à l'id envoyé en GET
            if(isset($_GET["id"])) {
                $article = new Article();
                $article_fetch = $article->getArticle($_GET["id"]);

                // Si l'id ne correspond à aucun article, une redirection est faite vers la page d'accueil
                if(!$article_fetch) {
                    header("Location: index.php?page=home");
                    exit();
                    
                } else {
                    // Sinon affichage du titre, image et contenu de l'article
                    echo "<h3>".$article->getTitle()."</h3>";
                    echo "<div class='article_img'><img src='assets/upload/".$article->getImage()."' alt='".$article->getTitle()."' ></div>";
                    echo "<div class='content_bloc'>".$article->getContent()."</div>";
                }
            }
        ?>
    </div>
</div>