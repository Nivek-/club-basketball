<div class='container'>
    <!-- TITLE OF THE PAGE -->
    <h3>Mentions légales</h3>

    <!-- LEGAL CONTENT -->
    <div class="legal_content">

        <!-- IDENTITY -->
        <h4>Identité</h4>
        <p>Nom du site web : Basketball d'Avesnes - Club féminin</p>
        <p>Adresse :</p>
        <p>Propriétaire : Club de basketball de la ville d'Avesnes</p>
        <p>Responsavle de publication :</p>
        <p>Conception et réalisation : Kévin Marquegnies</p>
        <p>Animation : </p>
        <p>Hébergement : 000Webhost</p>

        <!-- LEGAL ENTITY -->
        <h4>Personne morale</h4>
        <p>Club de basketball d'Avesnes ‘Adresse complète’</p>
        <p>‘Téléphone’ – 'adresse mail'</p>

        <!-- CONDITIONS OF USE -->
        <h4>Conditions d'utilisation</h4>
        <p>L’utilisation du présent site implique l’acceptation pleine et entière des conditions générales d’utilisation décrites ci-après.
            Ces conditions d’utilisation sont susceptibles d’être modifiées ou complétées à tout moment.
            </p>
        
        <!-- INFORMATIONS -->
        <h4>Informations</h4>
        <p>Les informations et documents du site sont présentés à titre indicatif, n’ont pas de caractère exhaustif, et ne peuvent engager la responsabilité du propriétaire du site.</p>
        <p>Le propriétaire du site ne peut être tenu responsable des dommages directs et indirects consécutifs à l’accès au site.</p>
        
        <!-- INTELLECTUAL PROPERTY -->
        <h4>Propriété intellectuelle</h4>
        <p>Sauf mention contraire, tous les éléments accessibles sur le site (textes, images, graphismes, logo, icônes, sons, logiciels, etc.) restent la propriété exclusive de leurs auteurs, en ce qui concerne les droits de propriété intellectuelle ou les droits d’usage.</p>
        <p>Toute reproduction, représentation, modification, publication, adaptation de tout ou partie des éléments du site, quel que soit le moyen ou le procédé utilisé, est interdite, sauf autorisation écrite préalable de l’auteur.</p>
        <p>Toute exploitation non autorisée du site ou de l’un quelconque des éléments qu’il contient est considérée comme constitutive d’une contrefaçon et poursuivie.</p>
        <p>Les marques et logos reproduits sur le site sont déposés par les sociétés qui en sont propriétaires.</p>
    </div>
</div>