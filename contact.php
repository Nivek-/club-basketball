<div class='container'>
    <!-- TITLE OF THE PAGE -->
    <h3>Nous contacter</h3>

    <!-- CONTACT PAGE BODY -->
    <div class='contact_body'>

        <!-- CONTACT INFORMATIONS -->
        <div class='contact_informations'>
            <p>Une question sur <a href='index.php?page=team'>nos équipes</a> ? Le club ? <a
                    href='index.php?page=tickets'>Les billets</a> ? Un autre renseignement ? Contactez-nous en
                remplissant
                le formulaire !</p>
            <img src='assets/images/women_holding_fake_basketball.jpg'
                alt="deux femmes assises dans un canapé tenant un téléphone et une feuille cartonée représentant un ballon de basket">
        </div>

        <!-- CONTACT FORM -->
        <form id='contact_form' method="POST" action="index.php?page=contact">
            <div class='form_input'>

                <!-- GENDER RADIOS -->
                <div class='radio_bloc'>
                    <div class='input_radio'>
                        <input type="radio" id="man" name="gender" value="Monsieur" >
                        <label for="man">Monsieur</label>
                    </div>
                    <div class='input_radio'>
                        <input type="radio" id="woman" name="gender" value="Madame">
                        <label for="woman">Madame</label>
                    </div>
                </div>

            </div>
            <div class='form_input'>
                <label for='lastname'>Nom</label>
                <input class='text_input' type="text" id="lastname" name='lastname' >
            </div>
            <div class='form_input'>
                <label for='firstname'>Prénom</label>
                <input class='text_input' type="text" id="firstname" name='firstname' >
            </div>
            <div class='form_input'>
                <label for='mail'>Adresse mail</label>
                <input class='text_input' type="email" id="mail" name='mail' >
            </div>
            <div class='form_input'>
                <label for='message'>Message</label>
                <textarea id='message' name='message'  maxlength="200"></textarea>
            </div>

            <!-- AGREE CHECKBOX -->
            <div class='form_input agree_checkbox'>
                <input type="checkbox" id="agree" name="agree" >
                <label for="agree">En soumettant ce formulaire j'accepte que les informations saisies soient utilisées
                    pour
                    permettre de me recontacter et d'établir des statistiques</label>
            </div>

            <div class="g-recaptcha" data-sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"></div>
            <p class='send_message'></p>

            <!-- SUBMIT BUTTON -->
            <input type="submit" value="Envoyer" name="submit_btn" class="valid_btn page_button">
        </form>
    </div>
</div>