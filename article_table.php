<?php

require_once "autoload.php";

// Importation de tous les articles en base de données
$article2 = new Article();
$article_results = $article2->getAll();

// Itération sur chaque élément afin d'afficher les informations de l'article dans une ligne du tableau de la page administration
foreach ($article_results as $result) {
    $article_date = date("d/m/Y", $result->getArticleDate());

    echo "<tr id='" . $result->getId() . "'><td>" . $result->getTitle() . "</td>";
    echo "<td class='hide_column'>" . $article_date . "</td>";
    echo "<td>";
    echo ($result->getPublish() == 0) ? "Non publié" : "Publié";
    echo "</td>";

    // Boutons permettant l'édition et la suppression d'un article
    echo "<td><i class='edit_article fas fa-edit'></i></td>";
    echo "<td><i class='delete_article fas fa-trash'></i></td></tr>";
}
