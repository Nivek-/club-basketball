<div class='container'>
    <!-- TITLE OF THE PAGE -->
    <h3>Plan du site</h3>

    <!-- SITEMAP SECTION -->
    <div class='sitemap'>

        <!-- ABOUT CATEGORY -->
        <div class='links_category'>
            <h4>A propos</h4>
            <ul>
                <li><a href="index.php?page=home">Accueil</a></li>
                <li><a href="index.php?page=history">Le basket féminin</a></li>
                <li><a href="index.php?page=team">Les équipes féminines</a></li>
                <li><a href="index.php?page=tickets">Les tarifs</a></li>
                <li><a href="index.php?page=contact">Contacts</a></li>
            </ul>
        </div>

        <!-- PLUS CATEGORY -->
        <div class='links_category'>
            <h4>Plus</h4>
            <ul>
                <li><a href="index.php?page=sitemap">Plan du site</a></li>
                <li><a href="index.php?page=legal">Mentions légales</a></li>
                <li><a href="index.php?page=privacy">Politique de confidentialité</a></li>
                <li><a href="index.php?page=admin">Administration</a></li>
            </ul>
        </div>
    </div>
</div>